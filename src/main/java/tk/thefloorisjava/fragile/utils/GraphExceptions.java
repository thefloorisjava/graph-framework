/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Tanya on 09.10.2015.
 */
package tk.thefloorisjava.fragile.utils;

public class GraphExceptions {
    private GraphExceptions(){}

    public static class InvalidGraphFileFormatException extends RuntimeException {
        public InvalidGraphFileFormatException(String message) {
            super(message);
        }
    }

    public static class WrongGraphTypeException extends Exception {
        public WrongGraphTypeException(String message) {
            super(message);
        }
    }
}

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 11.10.2015.
 */
package tk.thefloorisjava.fragile.utils;

import tk.thefloorisjava.fragile.graph.Edge;

import java.util.Comparator;

public class EdgeWeightComparator implements Comparator<Edge> {
    public int compare(Edge e1,Edge e2){
        return e1.getWeight() > e2.getWeight() ? +1 : e1.getWeight() < e2.getWeight() ? -1 : 0;
    }
}

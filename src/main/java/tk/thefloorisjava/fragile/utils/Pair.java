package tk.thefloorisjava.fragile.utils;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Aleksandra Marysheva on 07.10.2015.
 */
public class Pair  {

    private final int left;
    private final int right;

    public Pair(int left, int right)  throws Exception{
        this.left = left;
        this.right = right;
    }

    public int getLeft(){
        return left;
    }
    public int getRight(){
        return right;
    }


    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;

        Pair pair = (Pair)other;
        return pair.left == this.left && pair.right == this.right;
    }

    @Override
    public int hashCode() {
        int result = left;
        result = 31 * result + right;
        return result;
    }
}

package tk.thefloorisjava.fragile.graph;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import tk.thefloorisjava.fragile.utils.GraphExceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File must have following format:
 * 1 line : [matrix|list] - matrix if it describes matrix, list if it describes list
 * 2 line : [directed|not_directed]
 * if matrix:
 * 3 line: [int matrixSize] - size of matrix side
 * 4 line: <matrixSize> integers describing vertices' ids separated by whitespaces
 * next <matrixSize> lines: [int vertexId] [matrix line with [float weight] if there is an edge or [-] if there is not]
 * if list:
 *  [weighted|not_weighted]
 *    if weighted:
 *    for each vertex{
 *       [int] - vertex id
 *       for each adjusted vertex{
 *        [int] [float] - vertex id, edge weight
 *       }
 *       ;
 *     }
 *    if not weighted:
 *    for each vertex{
 *      list of ints describing start vertex and end vertices
 *    }
 *    ;
 */
public class GraphIO {
    private GraphIO(){}

    public static Graph CreateGraph( String filePath)  throws FileNotFoundException, GraphExceptions.InvalidGraphFileFormatException {
        Graph graph = new Graph();
        Scanner scanner = new Scanner(new File(filePath));
        try {
            String isMatrix = scanner.nextLine();
            String directionStatus = scanner.nextLine();
            if ("matrix".equalsIgnoreCase(isMatrix.trim())) {
                graph = initializeFromMatrix(graph, scanner);
            } else if ("list".equalsIgnoreCase(isMatrix.trim())) {
                String weightStatus = scanner.nextLine();

                if ("weighted".equalsIgnoreCase(weightStatus.trim())) {
                    graph = initializeFromWeightedList(graph, scanner);
                } else if ("unweighted".equalsIgnoreCase(weightStatus.trim())) {
                    graph = initializeFromNotWeightedList(graph, scanner);
                } else {
                    scanner.close();
                    throw new GraphExceptions.InvalidGraphFileFormatException("Not a \"weighted\" or \"unweighted\" list");
                }
            } else {
                scanner.close();
                throw new GraphExceptions.InvalidGraphFileFormatException("Not a \"matrix\" or a \"list\"");
            }

            graph.checkIfDirected();

            if (!((directionStatus.equalsIgnoreCase("directed") && graph.isDirected())
                    || (directionStatus.equalsIgnoreCase("not_directed") && !graph.isDirected()))) {
                throw new GraphExceptions.InvalidGraphFileFormatException("directivity of the graph does not correspond to the specified");
            }

            return graph;
        } catch (GraphExceptions.InvalidGraphFileFormatException e) {
            throw e;
        } finally {
            scanner.close();
        }
    }

    private static Graph initializeFromMatrix(Graph graph, Scanner scanner) throws GraphExceptions.InvalidGraphFileFormatException {
            int matrixSize, tempVertex;
            int edgeWeight;
            try {
                matrixSize = scanner.nextInt();
                int[] vertexArray = new int[matrixSize];
                for (int i = 0; i < matrixSize; ++i) {
                    tempVertex = scanner.nextInt();
                    graph.addVertex(tempVertex);
                    vertexArray[i] = tempVertex;
                }
                for (int i = 0; i < matrixSize; ++i) {
                    tempVertex = scanner.nextInt();
                    for (int j = 0; j < matrixSize; ++j) {
                        if (scanner.hasNextInt()) {
                            edgeWeight = scanner.nextInt();
                            graph.addVertex(tempVertex);
                            graph.addEdge(tempVertex, vertexArray[j], edgeWeight);
                        } else {
                            scanner.next();
                        }
                    }
                }

                return graph;
            } catch (Exception e) {
                throw new GraphExceptions.InvalidGraphFileFormatException("wrong matrix format");
            }
        }


    private static Graph  initializeFromWeightedList(Graph graph, Scanner scanner) throws GraphExceptions.InvalidGraphFileFormatException {
        int edgeWeight;
        int start;
        int end;
        Scanner stringScanner;
        try {
            scanner.useDelimiter(";");
            while (scanner.hasNext()) {
                stringScanner = new Scanner(scanner.next());
                start = stringScanner.nextInt();
                graph.addVertex(start);
                while (stringScanner.hasNext()) {
                    end = stringScanner.nextInt();
                    edgeWeight = stringScanner.nextInt();
                    graph.addVertex(end);
                    graph.addEdge(start, end, edgeWeight);
                }
            }
            return graph;
        } catch (Exception e) {
            throw new GraphExceptions.InvalidGraphFileFormatException("wrong weighted list format");
        }
    }

    private static Graph initializeFromNotWeightedList(Graph graph, Scanner scanner) throws GraphExceptions.InvalidGraphFileFormatException {
        int start;
        int end;
        Scanner stringScanner;
        try {
            scanner.useDelimiter(";");
            while (scanner.hasNext()) {
                stringScanner = new Scanner(scanner.next());
                start = stringScanner.nextInt();
                graph.addVertex(start);

                while (stringScanner.hasNext()) {
                    end = stringScanner.nextInt();
                    graph.addVertex(end);
                    graph.addEdge(start, end);
                }
            }
            return graph;
        } catch (Exception exception) {
            throw new GraphExceptions.InvalidGraphFileFormatException("wrong not weighted list format");
        }
    }

    public static void writeToFile(Graph graph, Graph.GraphType type, String path) {
        graph.checkIfDirected();
        try {
            File file = new File(path);
            FileWriter writer = new FileWriter(file);
            file.createNewFile();

            if (type == Graph.GraphType.MATRIX) {
                writeMatrixToFile(graph, writer);
            } else {
                if (graph.isWeighted())
                    writeToWeightedList(graph, writer);
                else
                    writeToNotWeightedList(graph, writer);
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeMatrixToFile(Graph graph, FileWriter fw) {
        Graph g = graph.prepareForMinCut();
        try {
            fw.write("matrix\n");
            if (graph.isDirected())
                fw.write("directed\n");
            else
                fw.write("not_directed\n");

            int vertSize = graph.getVerticesSize();

            fw.write(graph.getVertexCount() + "\n");

            IntArrayList tempVector = new IntArrayList();
            for (int i = 0; i < vertSize; ++i) {
                if (graph.getVertex(i) != null) {
                    tempVector.add(i);
                    fw.write(" " + i);
                }
            }
            fw.write("\n");
            System.out.print("\n");

            for (int i : tempVector) {
                fw.write(i + " ");
                for (int j : tempVector) {
                    if (g.vertexExists(i) && graph.getVertex(i).adjacentOut(j)) {
                        fw.write(graph.getVertex(i).getOutWeights(j).get(0) + " ");
                    } else {
                        fw.write("- ");
                    }
                }
                fw.write("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeToWeightedList(Graph graph, FileWriter fw) {
        try {
            fw.write("list\n");

            if (graph.isDirected())
                fw.write("directed\n");
            else
                fw.write("not_directed\n");

            fw.write("weighted\n");

            int vertSize = graph.getVerticesSize();
            IntArrayList tempVector = new IntArrayList();
            for (int i = 0; i < vertSize; ++i) {
                if (graph.getVertex(i) != null) {
                    tempVector.add(i);
                }
            }

            for (int i : tempVector) {
                fw.write(i + "\n");
                for (int j : graph.getOutEdges(i)) {
                    for(int  k : graph.getVertex(i).getOutWeights(j)) {
                        fw.write(j + " " + k + "\n");
                    }
                }
                fw.write(";\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeToNotWeightedList(Graph graph, FileWriter fw) {
        try {
            fw.write("list\n");

            if (graph.isDirected())
                fw.write("directed\n");
            else
                fw.write("not_directed\n");

            fw.write("not_weighted\n");

            int vertSize = graph.getVerticesSize();
            IntArrayList tempVector = new IntArrayList();
            for (int i = 0; i < vertSize; ++i) {
                if (graph.getVertex(i) != null) {
                    tempVector.add(i);
                }
            }

            for (int i : tempVector) {
                fw.write(i + " ");
                for (int j : graph.getOutEdges(i)) {
                    for(int  k : graph.getEdgeWeights(i, j)) {
                        fw.write(j + " ");
                    }
                }
                fw.write(";\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

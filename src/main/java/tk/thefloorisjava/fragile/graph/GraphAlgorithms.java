
package tk.thefloorisjava.fragile.graph;

import java.util.*;

import it.unimi.dsi.fastutil.ints.*;
import tk.thefloorisjava.fragile.utils.EdgeWeightComparator;
import tk.thefloorisjava.fragile.utils.GraphExceptions;
import tk.thefloorisjava.fragile.utils.Pair;
import tk.thefloorisjava.fragile.utils.GraphExceptions.WrongGraphTypeException;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by George on 05.10.2015.
 *
 * Contains methods to perform different algorithms on graphs.
 * After calling some algorithms, additional results may be stored in vertex vectors in graph instance.
 * For details check method's documentation.
 * All methods in this class are static.
 */
public class GraphAlgorithms {
    private GraphAlgorithms() {
    }

    /**
     * Method to calculate minimum cut in undirected graph.
     * If graph contains several connected components, algorithm will simply find one of them
     *
     * @param graph graph to calculate minimum cut of
     * @return number of edges of minimum cut
     */
    public static int minCut(Graph graph) throws GraphExceptions.WrongGraphTypeException {

        if (graph.isDirected()) {
            throw new GraphExceptions.WrongGraphTypeException("minimum cut cannot be used on directed graphs");
        }

        Graph g = graph.prepareForMinCut();

        int numberOfVertices = g.getNumberOfVertices();
        int bestMinCutWeight = Integer.MAX_VALUE;
        int prev = 0;
        IntList finalVertices = null;
        IntList[] constrictedVertices = new IntArrayList[numberOfVertices];

        for (int i = 0; i < numberOfVertices; ++i) {
            constrictedVertices[i] = new IntArrayList(numberOfVertices);
            constrictedVertices[i].add(i);
        }

        int[] w = new int[numberOfVertices];
        boolean[] exist = new boolean[numberOfVertices];
        boolean[] belongsToA = new boolean[numberOfVertices];
        Arrays.fill(exist, true);

        for (int phase = 0; phase < numberOfVertices - 1; ++phase) {
            Arrays.fill(belongsToA, false);
            Arrays.fill(w, 0);
            for (int iteration = 0; iteration < numberOfVertices - phase; ++iteration) {
                int selectedVertex = -1;
                for (int i = 0; i < numberOfVertices; ++i)
                    if (exist[i] && !belongsToA[i] && (selectedVertex == -1 || w[i] > w[selectedVertex])) {
                        selectedVertex = i;
                    }
                if (iteration == numberOfVertices - phase - 1) {
                    if (w[selectedVertex] < bestMinCutWeight) {
                        bestMinCutWeight = w[selectedVertex];
                        finalVertices = constrictedVertices[selectedVertex];
                    }
                    constrictedVertices[prev].addAll(constrictedVertices[prev].size(), constrictedVertices[selectedVertex].subList(0, constrictedVertices[selectedVertex].size()));
                    for (int i = 1; i < numberOfVertices; ++i) {
                        g.setSingleEdgeWeight(i, prev, g.getSingleEdgeWeight(i, prev) + g.getSingleEdgeWeight(selectedVertex, i));
                        g.setSingleEdgeWeight(prev, i, g.getSingleEdgeWeight(i, prev));
                    }
                    exist[selectedVertex] = false;
                } else {
                    belongsToA[selectedVertex] = true;
                    for (int i = 0; i < numberOfVertices; ++i) {
                        w[i] += g.getSingleEdgeWeight(selectedVertex, i);

                    }
                    prev = selectedVertex;
                }
            }
        }

        int minCutEdgesCount = 0;

        for(int i = 0; i < finalVertices.size(); ++i){
            int vertFrom = finalVertices.get(i);
             for(int vertTo : graph.getOutEdges(vertFrom)){
                if(!finalVertices.contains(vertTo)){
                    minCutEdgesCount++;
                }
             }
            finalVertices.remove(i);
        }

        return minCutEdgesCount;
    }

    /**
     * Safe method for executing breadth-first search.
     * Traverses entire graph from random vertex.
     * After execution vertexVector1 contains vertices by order of visiting.
     *
     * @param graph graph to perform bfs on
     * @return -3 because end was not specified
     */
    public static int bfs(Graph graph) {
        return bfs(graph, -1, -1);
    }

    /**
     * Overload of bfs method to specify start vertex.
     * Traverses entire graph from start vertex.
     * After execution vertexVector1 contains vertices by order of visiting.
     *
     * @param graph graph to perform bfs on
     * @param start id of start vertex
     * @return -3 because end was not specified, -1 if start is invalid
     */
    public static int bfs(Graph graph, int start) {
        return bfs(graph, start, -1);
    }

    /**
     * Overload of bfs method to specify start and end vertices.
     * Traverse graph from start vertex until end is found.
     * After execution vertexVector1 contains vertices on the path from start to end.
     * After execution vertexVector2 contains vertices by order of visiting.
     * After execution vertexVector3 contains vertices by order of exiting.
     *
     * @param graph graph to perform bfs on
     * @param start id of start vertex
     * @param end   id of end vertex
     * @return length of path, -1 if start/end is invalid, -2 if end vertex was not reached, -3 if end was not specified
     */
    public static int bfs(Graph graph, int start, int end) {//for unweighted graphs
        //using vertexHash as a "previous" hash for pairs of vertices and their predecessors
        graph.resetAlgorithmResults();

        start = checkStartId(graph, start);
        if (start < 0) {
            return -1;
        }

        if (end >= 0 && !graph.vertexExists(end)) {
            return -1;
        }

        bfsLoop(graph, start, end); //now we have pairs of vertices and their predecessors for vertex on the path to end

        return recordPath(graph, start, end); //determining path from start to end
    }

    /**
     * Unsafe method containing BFS traversing algorithm itself.
     * During execution fills list of visited vertices.
     * Should not be used outside BFS context.
     *
     * @param graph graph to perform BFS on
     * @param start start point
     * @param end   end point
     */
    private static void bfsLoop(Graph graph, int start, int end) {
        int current;
        IntList mentions;
        IntPriorityQueue queue = new IntArrayFIFOQueue();

        queue.enqueue(start);//inserting start vertex into queue, if it is valid
        graph.putInVertexMap(start, -1);
        graph.addVertexVector2(start);
        if (end >= 0 && start == end) {
            return;
        }

        while (!queue.isEmpty()) {
            current = queue.dequeueInt();
            graph.addVertexVector3(current);
            mentions = graph.getVertex(current).getOutEdges();
            for (int i = 0; i < mentions.size(); i++) { //for each adjacent vertex of predecessor vertex
                int id = mentions.get(i);
                if (graph.vertexMapContainsKey(id)) {
                    graph.cycleFound();
                }
                if (graph.shouldIgnore(id) || graph.vertexMapContainsKey(id)) { //if vertex was visited
                    continue;
                }
                queue.enqueue(id); //add it to queue
                graph.putInVertexMap(id, current); //store current vertex and its predecessor
                graph.addVertexVector2(id);
                if (end >= 0 && id == end) {
                    mentions.clear();
                    queue.clear();
                    graph.addVertexVector3(id);
                }
            }
            if (end < 0 && queue.isEmpty()) {
                int id = graph.getAnyVertexIdInList(graph.getVertexVector2(), false);
                if (id >= 0) {
                    queue.enqueue(id);
                }
            }
        }
    }

    static int dfs(Graph graph, int start, boolean exitIfStuck) {
        return dfs(graph, start, -1, exitIfStuck);
    }

    /**
     * Safe method to perform depth-first search on specified graph.
     * Traverses entire graph from random vertex.
     * After execution vertexVector2 contains vertices by order of visiting.
     * After execution vertexVector3 contains vertices by order of exiting.
     *
     * @param graph graph to perform bfs on
     * @return -3 because end was not specified
     */
    public static int dfs(Graph graph) {
        return dfs(graph, -1, -1, false);
    }

    /**
     * Overload of dfs method to specify start and end vertices.
     * Traverses entire graph from start vertex.
     * After execution vertexVector2 contains vertices by order of visiting.
     * After execution vertexVector3 contains vertices by order of exiting.
     *
     * @param graph graph to perform bfs on
     * @param start id of start vertex
     * @return -3 because end was not specified, -1 if start is invalid
     */
    public static int dfs(Graph graph, int start) {
        return dfs(graph, start, -1, false);
    }

    /**
     * Overload of dfs method to specify start and end vertices.
     * Traverse graph from start vertex until end is found.
     * After execution vertexVector1 contains vertices on the path from start to end.
     * After execution vertexVector2 contains vertices by order of visiting.
     * After execution vertexVector3 contains vertices by order of exiting.
     *
     * @param graph graph to perform bfs on
     * @param start id of start vertex
     * @param end   id of end vertex
     * @return length of path, or -1 if start or end do not exist, -2 if end vertex was not reached, -3 if not specified
     */
    public static int dfs(Graph graph, int start, int end) {
        return dfs(graph, start, end, false);
    }

    /**
     * Overload of dfs method to specify start and end vertices.
     * Traverse graph from start vertex until end is found.
     * After execution vertexVector1 contains vertices on the path from start to end.
     * After execution vertexVector2 contains vertices by order of visiting.
     * After execution vertexVector3 contains vertices by order of exiting.
     *
     * @param graph graph to perform bfs on
     * @param start id of start vertex
     * @param end   id of end vertex
     * @return length of path, or -1 if start or end do not exist, -2 if end vertex was not reached, -3 if not specified
     */
    private static int dfs(Graph graph, int start, int end, boolean exitIfStuck) {
        graph.resetAlgorithmResults();
        start = checkStartId(graph, start);
        if (start < 0) {
            return -1;
        }
        if (end >= 0 && !graph.vertexExists(end)) {
            return -1;
        }
        dfsLoop(graph, start, end, exitIfStuck);
        return recordPath(graph, start, end);
    }

    /**
     * Unsafe method used as a loop for dfs algorithm.
     * During execution fills list of visited vertices.
     * Should not be used outside DFS context.
     *
     * @param graph graph to perform algorithm on
     * @param start id of start vertex
     * @param end   id of end vertex
     */
    private static void dfsLoop(Graph graph, int start, int end, boolean exitIfStuck) {
        boolean nextStackElement;
        int current;
        IntList mentions;
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(start);
        graph.putInVertexMap(start, -1);
        graph.addVertexVector2(start);

        if (end >= 0 && start == end) {
            return;
        }
        while (!stack.isEmpty()) {
            current = stack.peek();
            mentions = graph.getVertex(current).getOutEdges();
            nextStackElement = false;
            for (int id : mentions) {
                if ((graph.isDirected() && stack.contains(id)) || !graph.isDirected() && graph.vertexMapContainsKey(id)) {
                    graph.cycleFound();
                }
                if (nextStackElement || (graph.shouldIgnore(id) && current == start) || graph.vertexMapContainsKey(id)) {
                    continue;
                }
                stack.push(id);
                graph.putInVertexMap(id, current);
                graph.addVertexVector2(id);
                nextStackElement = true;
                if (end >= 0 && id == end) {
                    stack.pop();
                    graph.addVertexVector3(id);
                    nextStackElement = false;
                }
            }
            if (!nextStackElement) {
                graph.addVertexVector3(current);
                stack.pop();
            }
            if (!exitIfStuck && end < 0 && stack.isEmpty()) {
                int id = graph.getAnyVertexIdInList(graph.getVertexVector2(), false);
                if (id >= 0) {
                    stack.push(id);
                    graph.putInVertexMap(id, current);
                    graph.addVertexVector2(id);
                }
            }
        }
    }

    /**
     * Safe method to determine if id of start vertex is adequate.
     * If start id is null, random vertex is selected as start one.
     *
     * @param graph graph to which start vertex belongs to
     * @param start id of start vertex
     * @return id of adequate start vertex, -1 if specified id does not exist
     */
    private static int checkStartId(Graph graph, int start) {
        /**
         * if start vertex was not specified
         *     set valid random vertex as a start
         *     exit if valid vertex not found
         * if vertex with this id does not exist return -1
         */
        if (start < 0) {
            start = graph.getAnyVertex();
            if (start < 0) {
                return -1;
            }
        } else if (!graph.vertexExists(start)) {
            return -1;
        }
        return start;
    }

    /**
     * Unsafe method to retrieve path from start to end just after BFS or DFS.
     * Should not be used outside the BFS/DFS context.
     *
     * @param graph graph on which BFS or DFS was executed
     * @param start start vertex
     * @param end   end vertex
     * @return length of path accounting for default edge weight, -1 in case of error
     */
    private static int recordPath(Graph graph, int start, int end) {
        int length = 0;
        int current;
        /**
         * if end vertex was specified
         *     if end vertex found
         *         starting from the end of the path
         *         while we haven't got to the start
         *             find vertex, previous to current
         *         return length of the path accounting for default edge weight
         *     if end vertex was not found - in case graph is disconnected
         * if end was not specified
         */

        if (end >= 0) {
            if (graph.vertexMapContainsKey(end)) {
                current = end;
                while (current != start) {
                    graph.addVertexVector1(0, current);
                    current = graph.getFromVertexMap(current);
                    length++;
                }
                graph.addVertexVector1(0, start);
                graph.clearVertexMap();
                return length * Graph.DEFAULT_EDGE_WEIGHT;
            } else {
                graph.clearVertexMap();
                return -2;
            }
        } else {
            graph.clearVertexMap();
            return -3;
        }
    }

    public static int doubleBfs(Graph graph, int start, int end) {
        boolean isFound;

        graph.resetAlgorithmResults();

        start = checkStartId(graph, start);
        if (start < 0) {
            return -1;
        }

        if (end >= 0 && !graph.vertexExists(end)) {
            return -1;
        }

        if (end >= 0 && start == end) {
            return 0;
        }

        IntPriorityQueue startQueue = new IntArrayFIFOQueue();
        IntPriorityQueue endQueue = new IntArrayFIFOQueue();

        startQueue.enqueue(start);
        endQueue.enqueue(end);
        graph.putInVertexMap(start, -1);
        graph.addVertexVector2(start);
        graph.addVertexVector3(end);

        while (true) {
            isFound = doubleBfsLoop(graph, startQueue, false);
            isFound = isFound || doubleBfsLoop(graph, endQueue, true);
            if (isFound) {
                break;
            } else if (startQueue.isEmpty()) {
                return -2;
            }
        }
        return recordPath(graph, start, end);
    }

    private static boolean doubleBfsLoop(Graph graph, IntPriorityQueue queue, boolean oppositeSearch) {
        if (queue.isEmpty()) {
            return false;
        }

        int current;
        IntList mentions;

        current = queue.dequeueInt();

        if (oppositeSearch) {
            mentions = graph.getVertex(current).getInEdges();
        } else {
            mentions = graph.getVertex(current).getOutEdges();
        }

        for (int i = 0; i < mentions.size(); i++) {
            int id = mentions.get(i);
            if (oppositeSearch ? graph.vertexVector3Contains(id) : graph.vertexMapContainsKey(id)) {
                graph.cycleFound();
                continue;
            }
            queue.enqueue(id);
            graph.putInVertexMap(oppositeSearch ? current : id, oppositeSearch ? id : current);
            if (oppositeSearch) {
                graph.addVertexVector3(id);
            } else {
                graph.addVertexVector2(id);
            }
            if (oppositeSearch ? graph.vertexMapContainsKey(id) : graph.vertexVector3Contains(id)) {
                mentions.clear();
                queue.clear();
                return true;
            }
        }
        return false;
    }

    /**
     * Method to perform topological sorting.
     * Using DFS method.
     *
     * @param graph graph we want to perform sorting on
     * @return List of identificators of vertices in sorted order (only one from existing set).
     */
    public static List<Integer> topoSort(Graph graph) throws Exception {
        if (!graph.isDirected())
            throw new WrongGraphTypeException("Graph should be Directed.");
        if (graph.isCyclic())
            throw new WrongGraphTypeException("Graph should not be Cyclic.");
        if (!graph.isConnected())
            throw new WrongGraphTypeException("Graph should be Connected");

        dfs(graph);
        IntList order = graph.getVertexVector3();
        List<Integer> result = new ArrayList<Integer>();
        for (int i = order.size() - 1; i >= 0; i--) {
            result.add(order.get(i));
        }
        return result;
    }

    /**
     * Method that finds the shortest path between any two vertices.
     * Uses auxiliary method restorePath().
     *
     * @param graph graph to perform algorithm on
     * @param start vertex, from which the root is looking from
     * @param end   vertex, to which the root is looking for
     * @return value, that is the weight of shortest path, -1 if vertices don't exist, -2 if there is no root
     */
    public static int shortestPath(Graph graph, int start, int end) throws Exception {

        if (!graph.vertexExists(start) || !graph.vertexExists(end)) {
            return -1;
        }
        if (dfs(graph, start, end) == -2) {
            return -2;
        }
        graph.resetAlgorithmResults();
        int n = graph.getNumberOfVertices();
        int[] parents = new int[n + 1];
        for (int i = 0; i < n + 1; i++) {
            parents[i] = 0;
        }
        IntList visitedVertices = new IntArrayList();
        IntList currentPath = new IntArrayList(Collections.nCopies(n + 1, Integer.MAX_VALUE));
        IntList vertices = graph.getAllVertices();
        currentPath.set(start, 0);
        for (int i = 0; i < n; ++i) {

            int v = -1;
            for (int j : vertices) {
                if ((!visitedVertices.contains(j)) && (v == -1 || currentPath.get(j) < currentPath.get(v))) {
                    v = j;
                }
            }

            if (currentPath.get(v) == Integer.MAX_VALUE)
                break;
            visitedVertices.add(v);
            Vertex ver = graph.getVertex(v);
            List<Pair> pairs = ver.getPairsMinWeights();
            for (Pair edge : pairs) {
                if (edge == null) continue;
                int to = edge.getLeft(),
                        len = edge.getRight();
                if (currentPath.get(v) + len < currentPath.get(to)) {
                    currentPath.set(to, currentPath.get(v) + len);
                    parents[to] = v;
                }
            }
        }
        restorePath(graph, parents, start, end);
        return currentPath.get(end);
    }

    /**
     * In the end of execution array "vertexVector1" in graph class contains shortest path from start to end
     *
     * @param graph   graph to perform algorithm on
     * @param parents array, that need to be processed to get shortest path
     * @return false, if array "parents" is empty
     */

    private static boolean restorePath(Graph graph, int[] parents, int start, int end) {
        if (parents.length == 0) {
            return false;
        }
        IntList path = new IntArrayList();
        for (int i = end; i != start; i = parents[i]) {
            path.add(i);
        }
        path.add(start);
        Collections.reverse(path);
        for (int i = 0; i < path.size(); ++i) {
            graph.addVertexVector1(path.get(i));
        }
        return true;
    }

    /**
     * A utility function to find set of an element i
     * (uses path compression technique)
     */
    private static int find(Int2ObjectMap<Pair> subsets, int i) throws Exception {
        int parent = subsets.get(i).getLeft();
        int rank = subsets.get(i).getRight();
        /**
         *  find root and make root as parent of i (path compression)
         */
        if (parent != i) {
            parent = find(subsets, parent);
            subsets.put(i, new Pair(parent, rank));
        }
        return parent;

    }

    /**
     * A function that does union of two sets of x and y
     * (uses union by rank)
     */
    private static void union(Int2ObjectMap<Pair> subsets, int x, int y) throws Exception {
        /**
         * Attach smaller rank tree under root of high rank tree
         * (Union by Rank)
         * If ranks are same, then make one as root and increment
         * its rank by one
         */
        int xroot = find(subsets, x);
        int yroot = find(subsets, y);

        int xParent = subsets.get(xroot).getLeft();
        int xRank = subsets.get(xroot).getRight();
        int yRank = subsets.get(yroot).getRight();

        if (xRank < yRank) {
            subsets.put(xroot, new Pair(yroot, xRank));
        } else if (xRank > yRank) {
            subsets.put(yroot, new Pair(xroot, yRank));
        }
        else {
            subsets.put(yroot, new Pair(xroot, yRank));
            subsets.put(xroot, new Pair(xParent, xRank + 1));
        }
    }

    /**
     * @param graph graph to perform algorithm on
     * @return list of edges that make MST
     * @throws WrongGraphTypeException
     */
    public static List<Edge> mstKruskal(Graph graph) throws Exception {
        /**
         *     Step 1:  Sort all the edges in non-decreasing order of their weight
         *     Create V subsets with single elements
         *     Number of edges to be taken is equal to V-1
         *         Step 2: Pick the smallest edge. And increment the index
         *         for next iteration
         *         If including this edge doesn't cause cycle, include it
         *         in result and increment the index of result for next edge
         */
        if (graph.isDirected()) {
            throw new WrongGraphTypeException("Unexpected graph type: directed");
        } else if (!graph.isConnected()) {
            throw new WrongGraphTypeException("Unexpected graph type: disconnected");
        } else {
            int vCount = graph.getNumberOfVertices();
            List<Edge> result = new ArrayList<Edge>(vCount);
            int e = 0;
            int i = 0;
            int id;
            List<Edge> edges = graph.getEdgesList();
            Int2ObjectMap<Pair> subsets = new Int2ObjectOpenHashMap<Pair>();
            IntList ids = graph.getAllVertices();

            edges.sort(new EdgeWeightComparator());
            for (int v = 0; v < vCount; v++) {
                id = ids.get(v);
                subsets.put(id, new Pair(id, 0));
            }
            while (e < vCount - 1) {
                Edge nextEdge = edges.get(i++);
                int x = find(subsets, nextEdge.getStartVertex());
                int y = find(subsets, nextEdge.getEndVertex());
                if (x != y) {
                    result.add(e++, nextEdge);
                    union(subsets, x, y);
                }
            }
            return result;
        }
    }

    /**
     * Implementation of algorithm, that compute strong components of the graph.
     *
     * @param graph graph to perform algorithm on
     * @return empty set, if it's not possible, other way HashMap of strong components and their indexes
     */
    public static Map<Integer, List<Integer>> computingStrongComponents(Graph graph) throws WrongGraphTypeException {

        if (!graph.isDirected())
            throw new WrongGraphTypeException("Unexpected graph type: directed");
        if (graph.getNumberOfVertices() == 0) {
            return new HashMap<Integer, List<Integer>>();
        }

        Graph graph1 = new Graph();
        graph1.makeDirected(true);

        graph.resetAlgorithmResults();
        graph1.resetAlgorithmResults();

        int n = graph.getVerticesSize();
        IntList used = new IntArrayList();
        IntList order = new IntArrayList();
        HashMap<Integer, List<Integer>> components = new HashMap<Integer, List<Integer>>();
        List<Integer> component;
        int numberOfComponents = 1;

        IntList vertices = graph.getAllVertices();

        for (int i : vertices) {
            graph1.addVertex(graph.getVertexValue(i));
        }
        //TODO: next cycle may be optimized by using already known data
        for (int i = 0; i < n; i++) {
            Vertex v = graph.getVertex(i);
            if (v == null) {
                n++;
                continue;
            }
            IntList list = new IntArrayList(v.getOutEdges());
            for (int j = 0; j < list.size(); ++j) {
                graph1.addEdge(list.get(j), i);
            }
        }

        for (int i = 0; i < n; ++i) {
            if (!used.contains(i)) {
                dfs(graph, i);
                order.addAll(graph.getVertexVector3());
                used.addAll(graph.getVertexVector3());
            }
        }

        used.clear();

        for (int i = 0; i < n; ++i) {
            int v = order.get(n - 1 - i);
            if (!graph1.shouldIgnore(v)) {
                GraphAlgorithms.dfs(graph1, v, true);
                graph1.addVerticesToIgnore(graph1.getVertexVector2());
                component = graph1.getVertexVector2();
                components.put(numberOfComponents++, new ArrayList<Integer>(component));
                component.clear();
            }
        }
        graph.resetIgnoreList();
        return components;
    }
}

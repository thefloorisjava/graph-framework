package tk.thefloorisjava.fragile.graph;

import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;

import java.util.*;

import it.unimi.dsi.fastutil.ints.*;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Tanya on 30.09.2015.
 *
 * This class contains graph representation in memory and methods for performing standard operations on it.
 * FRAGiLE allows multiple edges between same vertices, directed/undirected and weighted/unweighted graphs.
 *
 * Graph is stored as an array of Vertex objects.
 * Graph structure is designed for fast access to vertices and edges.
 * Accessing vertices and edges by integer id is an O(1) operation, accessing by label takes linear time.
 *
 * Graph is classified by directivity and weight properties.
 * Optionally uniqueness of vertex values and dynamic id assignment for better memory management can be forced.
 * To perform algorithms, use methods from {@link GraphAlgorithms} and pass instance of this class.
 */
public class Graph {
    private boolean isDirected;
    private boolean isWeighted;
    private boolean isCyclic;

    private int vertexCount;
    private long edgeCount;
    private List<Vertex> vertices;
    /**
     * list of unoccupied vertex id's to choose from, sorted in decrement order
     */
    private IntList freeVertices;
    /**
     * stores results of certain algorithms
     */
    private IntList vertexVector1;
    /**
     * stores results of certain algorithms
     */
    private IntList vertexVector2;
    /**
     * stores results of certain algorithms
     */
    private IntList vertexVector3;
    /**
     * stores intermediate results of certain algorithms
     */
    private Int2IntMap vertexMap;
    private IntList verticesToIgnore;
    /**
     * representation of graph vertices and edges outside framework
     */

    private long weightedEdgesCount;
    private int connectedComponentsCount;
    private IntList freeComponentIds;
    private IntList componentSizes;

    private boolean cyclicMayHaveChanged;

    private boolean dynamicIdEnabled;
    /**
     * enforces all vertices to have unique values and forbids null values
     */
    private boolean uniqueVertexValues;

    private List<Integer> idChangeHistory;
    private List<Integer> reverseIdChangeHistory;

    public static final int DEFAULT_EDGE_WEIGHT = 1;

    public enum GraphType {
        MATRIX, LIST
    }

    public Graph() {
        isDirected = true;
        isWeighted = false;
        isCyclic = false;

        vertexCount = 0;
        edgeCount = 0;
        vertices = new ArrayList<Vertex>();
        freeVertices = new IntArrayList();
        vertexVector1 = new IntArrayList();
        vertexVector2 = new IntArrayList();
        vertexVector3 = new IntArrayList();
        vertexMap = new Int2IntOpenHashMap();
        verticesToIgnore = new IntArrayList();

        weightedEdgesCount = 0;
        connectedComponentsCount = 0;
        freeComponentIds = new IntArrayList();
        componentSizes = new IntArrayList();

        cyclicMayHaveChanged = true;

        dynamicIdEnabled = false;
        uniqueVertexValues = false;

        idChangeHistory = new ArrayList<Integer>();
        reverseIdChangeHistory = new ArrayList<Integer>();
    }

    void setVertices(List<Vertex> vertices){
        this.vertices = vertices;
    }
    
    public IntList addVertices(int count) {
        IntList result = new IntArrayList();
        for(int i = 0; i < count; i++) {
            result.add(addVertex(null));
        }
        return result;
    }

    /**
     * Safe method to add new vertex to graph.
     *
     * @return int id of created vertex
     */
    public int addVertex() {
        return addVertex(null);
    }

    /**
     * Overload of default addVertex to assign vertex to a specified id.
     *
     * @param id desired id of vertex
     * @return   int id of created vertex or -1 in case this id is occupied
     */
    public int addVertex(int id){
        return addVertex(id, null);
    }

    /**
     * Overload of default addVertex to assign String value to a vertex.
     *
     * @param value desired value of a vertex
     * @return      int id of created vertex
     */
    public int addVertex(String value) {
        int id = calculateNewId(freeVertices, vertexCount);
        return addVertex(id, value);
    }

    /**
     * Overload of default addVertex to assign vertex with specified value to a specified id.
     *
     * @param id    desired id of vertex
     * @param value desired value of a vertex
     * @return      int id of created vertex or -1 if this id is occupied
     */
    public int addVertex(int id, String value) {
        if (!checkVertexValue(value)) {
            return -1;
        }
        /**
         * if desired id is bigger than biggest available id
         *     each vertex between existing ones and one with desired id
         *         mark as available
         *         initialize as non-existing
         *     A block responsible for keeping graph parameters updated
         * if id is positive - it's valid
         *     if this id is not occupied
         *         insert vertex at desired location
         *         A block responsible for keeping graph parameters updated
         *     if vertex with this id already exists
         * if vertex id is negative - invalid
         */
        if(id >= vertices.size()){
            int i;
            for(i = vertices.size(); i < id; i++){
                freeVertices.add(i);
                vertices.add(null);
            }
            vertices.add(new Vertex(value));
            vertexCount++;
            resetAlgorithmResults();
            assignVertexComponentId(id);
            return id;
        }
        else if(id >= 0){
            if (vertices.get(id) == null) {
                vertices.set(id, new Vertex(value));
                vertexCount++;
                resetAlgorithmResults();
                assignVertexComponentId(id);
                return id;
            }
            else{
                return -1;
            }
        }
        else{
            return -1;
        }
    }

    private boolean checkVertexValue(String value) {
        return value == null || !dynamicIdEnabled || getId(value) < 0;
    }

    public int addVertexOnEdge(int idFrom, int idTo){
        int vertexId = calculateNewId(freeVertices, vertexCount);
        return addVertexOnEdge(idFrom, idTo, DEFAULT_EDGE_WEIGHT, vertexId, null);
    }

    public int addVertexOnEdge(int idFrom, int idTo, int weight){
        int vertexId = calculateNewId(freeVertices, vertexCount);
        return addVertexOnEdge(idFrom, idTo, weight, vertexId, null);
    }

    public int addVertexOnEdge(int idFrom, int idTo, int weight, int vertexId) {
        return addVertexOnEdge(idFrom, idTo, weight, vertexId, null);
    }

    public int addVertexOnEdge(int idFrom, int idTo, int weight, String vertexValue){
        int vertexId = calculateNewId(freeVertices, vertexCount);
        return addVertexOnEdge(idFrom, idTo, weight, vertexId, vertexValue);
    }

    /**
     * Method to add vertex that lies on specified edge.
     *
     * @param idFrom id of start vertex of specified edge
     * @param idTo id of end vertex of specified edge
     * @param weight weight of specified edge
     * @param vertexId id of desired vertex
     * @param vertexValue value of desired vertex
     * @return id of created vertex
     */
    public int addVertexOnEdge(int idFrom, int idTo, int weight, int vertexId, String vertexValue){
        removeEdge(idFrom, idTo, weight);
        int newId = addVertex(vertexId, vertexValue);
        addEdge(idFrom, newId, weight);
        addEdge(newId, idTo, weight);
        return newId;
    }


    /**
     * Safe method to remove vertex with specified id and all edges, connected to it.
     *
     * @param id desired id of vertex
     * @return       true if vertex with given id existed and was deleted, false if it did not exist
     */
    public boolean removeVertex(int id) {
        if(vertexExists(id)){
            removeAllMentions(id);
            int componentId = vertices.get(id).getComponentId();
            vertices.set(id, null);
            vertexCount--;
            insertIntInOrder(id, freeVertices);
            resetAlgorithmResults();
            if (dynamicIdEnabled) {
                if (id < vertexCount) {
                    replaceVertexId(vertexCount, id);
                    updateIdChangeHistory(vertexCount, id);
                }
                vertices.remove(vertexCount);
            }
            /**
             * A block responsible for keeping graph parameters updated
             */
            componentSizes.set(componentId, componentSizes.get(componentId) - 1);
            if (componentSizes.get(componentId) == 0) {
                freeComponentIds.add(componentId);
                connectedComponentsCount--;
            }
            removeZeroElementsAtEnd(componentSizes);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to store changes in vertices id's after automatic id changes.
     *
     * @param oldId
     * @param newId
     */
    private void updateIdChangeHistory(int oldId, int newId) {
        Integer prev;
        /**
         * delete entry associated with id of deleted vertex if entry existed
         * if newId returns oldId to original value
         *     remove corresponding entry
         * if no entries with same oldId detected
         *     adding new change entry
         * otherwise update already existing entry
         * */
        if (newId < reverseIdChangeHistory.size()) {
            prev = reverseIdChangeHistory.get(newId);
            if (prev != null) {
                idChangeHistory.set(prev, null);
                reverseIdChangeHistory.set(newId, null);
                removeEmptyElementsAtEnd(idChangeHistory);
                removeEmptyElementsAtEnd(reverseIdChangeHistory);
            }
        }
        if (newId < idChangeHistory.size()) {
            prev = idChangeHistory.get(newId);
            if (prev != null && prev.equals(oldId)) {
                idChangeHistory.set(newId, null);
                reverseIdChangeHistory.set(oldId, null);
                removeEmptyElementsAtEnd(idChangeHistory);
                removeEmptyElementsAtEnd(reverseIdChangeHistory);
                return;
            }
        }
        if (oldId >= reverseIdChangeHistory.size() || reverseIdChangeHistory.get(oldId) == null) {
            setIntAtPosition(newId, oldId, idChangeHistory);
            setIntAtPosition(oldId, newId, reverseIdChangeHistory);
        } else {
            prev = reverseIdChangeHistory.get(oldId);

            idChangeHistory.set(prev, newId);
            reverseIdChangeHistory.set(oldId, null);
            removeEmptyElementsAtEnd(reverseIdChangeHistory);
            setIntAtPosition(prev, newId, reverseIdChangeHistory);
        }
    }

    private void setIntAtPosition(int integer, int index, List<Integer> list) {
        if (index < list.size()) {
            list.set(index, integer);
        } else {
            int count = index - list.size();
            for (int i = count; i > 0; i--) {
                list.add(null);
            }
            list.add(integer);
        }
    }

    private void removeEmptyElementsAtEnd(List list) {
        while (list.size() > 0 && list.get(list.size()-1) == null) {
            list.remove(list.size()-1);
        }
    }
    private void removeZeroElementsAtEnd(IntList list) {
        while (componentSizes.size() > 0 && componentSizes.get(componentSizes.size()-1) == 0) {
            componentSizes.remove(componentSizes.size() - 1);
        }
    }

    /**
     * Unsafe method to insert int in list sorted in decrementing order.
     * Implemented as a binary search.
     *
     * @param id
     * @param list
     */
    private void insertIntInOrder(int id, IntList list) {
        int imin = 0;
        int imax = list.size();
        while (imin > imax)
        {
            int imid = (imin + imax) / 2;
            if(imid >= imax) {
                break;
            }
            if (list.get(imid) > id){
                imin = imid + 1;
            } else{
                imax = imid;
            }
        }
        list.add(imin, id);
    }

    private void assignVertexComponentId(int id) {
        int componentId = calculateNewId(freeComponentIds, connectedComponentsCount);
        vertices.get(id).setConnectedComponentId(componentId);
        if (componentSizes.size() == connectedComponentsCount) {
            componentSizes.add(1);
        } else {
            componentSizes.set(componentId, componentSizes.get(componentId) + 1);
        }
        connectedComponentsCount++;
    }

    /**
     * Unsafe method to remove all edges, connected to a vertex with given id.
     *
     * @param vertexId id of vertex which mentions in other vertices should be deleted
     */
    private void removeAllMentions(int vertexId) {
        Vertex vertex = vertices.get(vertexId);
        for (int i : vertex.getInEdges()) {
            IntList weights = vertices.get(i).getOutWeights(vertexId);
            for (int j = 0; j < weights.size(); j++) {
                int weight = weights.get(j);
                removeEdge(i, vertexId, weight);
            }
        }
        for (int i : vertex.getOutEdges()) {
            IntList weights = vertex.getOutWeights(i);
            for (int j = 0; j < weights.size(); j++) {
                int weight = weights.get(j);
                removeEdge(vertexId, i, weight);
            }
        }
    }

    /**
     * Safe method to add an edge with default weight.
     *
     * @param idFrom id of starting vertex of an edge
     * @param idTo   id of ending vertex of an edge
     * @return       true if vertices id's were correct and edge was created, otherwise false
     */
    public boolean addEdge(int idFrom, int idTo){
        return addEdge(idFrom, idTo, Graph.DEFAULT_EDGE_WEIGHT);
    }

    /**
     * Safe method to add an edge with specified weight.
     *
     * @param idFrom id of starting vertex of an edge
     * @param idTo   id of ending vertex of an edge
     * @param weight desired weight of an edge
     * @return       true if vertices id's were correct and edge was created, otherwise false
     */
    public boolean addEdge(int idFrom, int idTo, int weight) {
        Vertex v1, v2;
        /**
         * checking if vertices exist
         */
        if (vertexExists(idFrom) && vertexExists(idTo)) {
            v1 = vertices.get(idFrom);
            v2 = vertices.get(idTo);
            v1.addOutEdge(idTo, weight);
            v2.addInEdge(idFrom);
            if(!isDirected) {
                v2.addOutEdge(idFrom, weight);
                v1.addInEdge(idTo);
            }
            edgeCount++;
            resetAlgorithmResults();
            /**
             * A block responsible for keeping graph parameters updated
             */
            if(weight != Graph.DEFAULT_EDGE_WEIGHT) {
                isWeighted = true;
                weightedEdgesCount++;
            }

            if(edgeInSingleComponent(idFrom, idTo)) {
                if (!isDirected) {
                    isCyclic = true;
                } else {
                    cyclicMayHaveChanged = true;
                }
            } else {
                mergeComponents(vertices.get(idFrom).getComponentId(), vertices.get(idTo).getComponentId());
                connectedComponentsCount--;
            }

            removeZeroElementsAtEnd(componentSizes);
            return true;
        } else{
            return false;
        }
    }

    /**
     * Safe method to remove edge with specified start and end vertex.
     *
     * @param idFrom id of start vertex
     * @param idTo   id of end vertex
     * @return       true if edge was deleted, false if edge did not exist
     */
    public boolean removeEdge(int idFrom, int idTo) {
        return removeEdge(idFrom, idTo, Graph.DEFAULT_EDGE_WEIGHT);
    }

    /**
     * Overload of removeEdge to remove edge with specified start, end vertex and weight.
     *
     * @param idFrom id of start vertex
     * @param idTo   id of end vertex
     * @param weight weight of an edge
     * @return       true if edge was deleted, false if edge did not exist
     */
    public boolean removeEdge(int idFrom, int idTo, int weight) {
        boolean deletedReverseEdge = false;
        Vertex v1, v2;
        if (vertexExists(idFrom) && vertexExists(idTo)) { //checking if vertices exist
            v1 = vertices.get(idFrom);
            v2 = vertices.get(idTo);
            if (v1.remOutEdge(idTo, weight)) { //checking if deletion was successful
                v2.remInEdge(idFrom);
                //TODO: add check which accounts possible number of weights of in edges
                edgeCount--;
                if(!isDirected) {
                    v1.remInEdge(idTo);
                    deletedReverseEdge = v2.remOutEdge(idFrom, weight);
                    edgeCount--;
                }
                //TODO: check if next condition should be replaced with !isDirected

                if(!deletedReverseEdge) {
                    edgeCount++;
                }

                resetAlgorithmResults();
                /**
                 * A block responsible for keeping graph parameters updated
                 */
                if(weight != Graph.DEFAULT_EDGE_WEIGHT) {
                    weightedEdgesCount--;
                }
                if(weightedEdgesCount == 0) {
                    isWeighted = false;
                }

                IntList verticesToSeparate = isCoupled(idFrom, idTo);
                /**
                 * if edge divided connected component
                 */
                if(!verticesToSeparate.isEmpty()) {
                    separateVertices(verticesToSeparate);
                } else if (isCyclic) {
                    cyclicMayHaveChanged = true;
                }
                resetAlgorithmResults();
                return true;
            } else{
                return false;
            }
        } else{
            return false;
        }
    }

    /**
     * Unsafe method to check whether two vertices are in same connected component.
     *
     * @param idFrom id of first vertex
     * @param idTo id of second vertex
     * @return list of vertices that should be moved to separate connected component
     */
    private IntList isCoupled(int idFrom, int idTo) {
        Graph graph;
        if (isDirected) {
            int componentId = vertices.get(idFrom).getComponentId();
            graph = new Graph();
            graph.makeDirected(true);
            for (int i = 0; i < vertices.size(); i++) {
                Vertex vertex = vertices.get(i);
                if (vertex == null) {
                    graph.vertices.add(null);
                    continue;
                }
                if (vertex.getComponentId() == componentId) {
                    graph.vertices.add(new Vertex(vertex));
                } else {
                    graph.vertices.add(null);
                }
            }
            graph.optimizeMemory();
            graph.makeDirected(false);
        } else {
            graph = this;
        }
        int length = GraphAlgorithms.dfs(graph, idTo, idFrom);
        if (!isDirected) {
            isCyclic = graph.isCyclic;
            cyclicMayHaveChanged = graph.cyclicMayHaveChanged;
        }
        if (length < 0) {
            return new IntArrayList(graph.vertexVector2);
        } else {
            return new IntArrayList();
        }
    }

    private void separateVertices(IntList vertexList){
        int oldId = vertices.get(vertexList.get(0)).getComponentId();

        int newId = calculateNewId(freeComponentIds, connectedComponentsCount);
        for (int vertex : vertexList) {
            vertices.get(vertex).setConnectedComponentId(newId);
        }
        connectedComponentsCount++;

        componentSizes.set(oldId, componentSizes.get(oldId) - vertexList.size());
        if (componentSizes.size() > newId) {
            componentSizes.set(newId, vertexList.size());
        } else {
            for (int i = newId - componentSizes.size(); i > 0; i--) {
                componentSizes.add(0);
            }
            componentSizes.add(vertexList.size());
        }
    }

    /**
     * Safe method to check whether specified edge exists.
     *
     * @param idFrom id of start vertex of the edge
     * @param idTo id of end vertex of the edge
     * @param weight weight of the edge
     * @return true if edge exists, otherwise false
     */
    public boolean edgeExists(int idFrom, int idTo, int weight) {
        Vertex vertex = vertices.get(idFrom);
        return vertex != null && vertex.getOutWeights(idTo).contains(weight);

    }

    private boolean edgeInSingleComponent(int idFrom, int idTo) {
        return vertices.get(idFrom).getComponentId() == vertices.get(idTo).getComponentId();
    }

    /**
     * Unsafe method to merge component with less vertices with component with more vertices.
     *
     * @param component1 id of first connected component
     * @param component2 id of second connected component
     */
    private void mergeComponents(int component1, int component2) {
        if(componentSizes.get(component1).compareTo(componentSizes.get(component2)) < 0) {
            moveVerticesToComponent(component1, component2);
        } else {
            moveVerticesToComponent(component2, component1);
        }
    }

    private void moveVerticesToComponent(int oldId, int newId) {
        componentSizes.set(newId, componentSizes.get(newId) + componentSizes.get(oldId));
        int j = componentSizes.get(oldId);
        for (int i = 0; i < vertices.size(); i++) {
            if (j == 0) {
                break;
            }
            Vertex vertex = vertices.get(i);
            if(vertex != null && vertex.getComponentId() == oldId) {
                vertex.setConnectedComponentId(newId);
                j--;
            }
        }
        componentSizes.set(oldId, 0);
    }

    public boolean isAdjacent(int idFrom, int idTo) {
        if (vertexExists(idFrom) && vertexExists(idTo)) {
            Vertex from = vertices.get(idFrom);
            return from.adjacentOut(idTo);
        } else{
            return false;
        }
    }

    /**
     * Safe method to check if this graph is weighted.
     *
     * @return true if graph is weighted, otherwise false
     */
    public boolean checkIfWeighted() {
        isWeighted = false;
        for(Vertex i : vertices){
            if (i.checkIfWeighted()) {
                isWeighted = true;
                break;
            }
        }
        return isWeighted;
    }

    void setVertexVector1(IntList vector) {
        vertexVector1 = vector;
    }

    boolean addVertexVector1(int id) {
        return vertexVector1.add(id);
    }

    void addVertexVector1(int position, int id) {
        vertexVector1.add(position, id);
    }

    boolean  addVertexVector2(int id) {
        return vertexVector2.add(id);
    }

    boolean addVertexVector3(int id) {
        return vertexVector3.add(id);
    }

    public IntList getVertexVector1() {
        return vertexVector1;
    }

    public IntList getVertexVector2() {
        return vertexVector2;
    }

    public IntList getVertexVector3() {
        return vertexVector3;
    }

    public boolean vertexVector2Contains(int i) {
        return vertexVector2.contains(i);
    }

    public boolean vertexVector3Contains(int i){
        return vertexVector3.contains(i);
    }

    boolean putInVertexMap(int id1, int id2){
        return vertexMap.put(id1, id2) < 0;
    }

    boolean vertexMapContainsKey(int id) {
        return vertexMap.containsKey(id);
    }

    int getFromVertexMap(int id){
        return vertexMap.get(id);
    }

    void clearVertexMap() {
        vertexMap.clear();
    }

    public int getConnectedComponentsCount() {
        return connectedComponentsCount;
    }

    void addVerticesToIgnore(IntList list) {
        verticesToIgnore.addAll(list);
    }

    boolean shouldIgnore(int id) {
        return verticesToIgnore.contains(id);
    }

    void resetIgnoreList() {
        verticesToIgnore.clear();
    }

    public void enableDynamicId(boolean state) {
        dynamicIdEnabled = state;
        if (state) {
            optimizeMemory();
        }
    }

    /**
     * Returns new id of vertex with specified id, -1 if id did not change.
     *
     * @param oldId
     * @return
     */
    public int peekIdChange(int oldId) {
        if (oldId > 0 && oldId < idChangeHistory.size() && idChangeHistory.get(oldId) != null) {
            return idChangeHistory.get(oldId);
        } else {
            return -1;
        }
    }

    public void clearIdChangeHistory() {
        idChangeHistory.clear();
        reverseIdChangeHistory.clear();
    }

    public boolean enableUniqueVertexValue(boolean state) {
        if (uniqueVertexValues) {
            if (!state) {
                uniqueVertexValues = false;
                return true;
            }
        } else {
            if (state) {
                if (vertexValuesUnique()) {
                    uniqueVertexValues = true;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns list of all changed id's, index is old id, value is new id.
     * Only automatic id changes are tracked.
     *
     * @return
     */
    public List<Integer> getIdChangeHistory() {
        List<Integer> result = new ArrayList<Integer>(idChangeHistory);
        clearIdChangeHistory();
        return result;
    }

    /**
     * Safe method for clearing containers of additional algorithms' results.
     */
    public void resetAlgorithmResults() {
        vertexVector1.clear();
        vertexVector2.clear();
        vertexVector3.clear();
        vertexMap.clear();
    }

    /**
     *@see #vertexExists(int)
     * @return null - if vertex does not exist, "" - if the value of needed vertex is null,
     *         not empty String (actual value) - if vertex has a value
     */
    public String getVertexValue(int vertexId) {

        if(vertexExists(vertexId)) {
            Vertex vertex = vertices.get(vertexId);
            if(vertex.getValue() == null)
                return "";
            else
                return vertex.getValue();
        }
        else return null;
    }

    /**
     *@see #vertexExists(int)
     * @return previous value - if needed vertex exists
     *          null - if needed vertex does not exist
     */
    public String setVertexValue(int vertexId, String value) {
        if(vertexExists(vertexId)) {

            Vertex vertex = vertices.get(vertexId);
            String prevValue = vertex.getValue();
            vertex.setValue(value);

            return prevValue;
        }
        else
            return null;
    }

    /**
     * @return vertex id or in case there are no vertices returns -1;
     */
    public int getAnyVertex() {
        /**
         * if there is at least one vertex
         */
        if (vertexCount > 0) {//
            for(Vertex vertex : vertices){
                if (vertex != null) {
                    /**
                     * find first valid vertex
                     */
                    return vertices.indexOf(vertex);
                }
            }
            return -1;
        } else{
            return -1;
        }
    }

    /**
     * Safe method to get id's of all vertices that are adjacent to a given vertex.
     *
     * @param value string value of specified vertex
     * @return   list of id's of adjacent vertices
     */
    public List<Integer> getMentions(String value) {
        return getMentions(getId(value));
    }

    /**
     * Safe method to get id's of all vertices that are adjacent to a given vertex.
     *
     * @param id id of specified vertex
     * @return   list of id's of adjacent vertices
     */
    public List<Integer> getMentions(int id) {
        Vertex vertex = getVertex(id);
        if (vertex != null) {
            return vertex.getMentions();
        } else{
            return null;
        }
    }

    /**
     * Unsafe method to get id for new vertex for an optimal usage of available id's.
     * Should not be called outside of context of creating a vertex.
     *
     * @return optimal id for a new vertex
     */
    private int calculateNewId(IntList freeIdPool, int idCount) {
        if (!freeIdPool.isEmpty()) {
            return freeIdPool.remove(freeIdPool.size() - 1);
        } else return idCount;
    }

    /**
     * Safe method to check if vertex with needed id exists.
     *
     * @param vertexId id of the needed vertex
     * @return         true if needed vertex exists, false if wanted vertex does not exist
     */
    public boolean vertexExists(int vertexId){
        return getVertex(vertexId) != null;
    }

    public boolean vertexExists(String value){
        return getId(value) >= 0;
    }

    public boolean vertexExists(Vertex vertex){
        return getId(vertex) >= 0;
    }

    /**
     * Safe method to get Vertex object with specified int id.
     *
     * @param  vertexId id of the needed vertex
     * @return          Vertex object with specified id or null if such vertex does not exist
     */
    Vertex getVertex(int vertexId) {
        if(vertexIdLegit(vertexId)){
            return vertices.get(vertexId);
        } else {
            return null;
        }
    }

    Vertex getVertex(String value) {
        int vertexId = getId(value);
        if(vertexIdLegit(vertexId)){
            return vertices.get(vertexId);
        } else {
            return null;
        }
    }

    /**
     * If unique vertex values disabled search for null values is allowed.
     *
     * @param value
     * @return
     */
    public int getId(String value) {
        long hashCode = computeLongHash(value);
        int count = vertices.size();
        Vertex vertex;
        for (int i = 0; i < count; i++) {
            vertex = getVertex(i);
            if (vertex == null || !vertex.valueHashCodeEquals(hashCode)) {
                continue;
            }
            if (value != null) {
                if (value.equals(vertex.getValue())) {
                    return i;
                }
            } else {
                if (!uniqueVertexValues && vertex.getValue() == null) {
                    return i;
                }
            }
        }
        return -1;
    }

    public int getId(Vertex v) {
        int count = vertices.size();
        for (int i = 0; i < count; i++) {
            if (v == getVertex(i)) {
                return i;
            }
        }
        return -1;
    }

    public List<Integer> getIds(String... _values) {
        List<String> values = new ArrayList<String>();
        for (String str : _values) {
            values.add(str);
        }
        return getIdsByString(values);
    }

    public List<Integer> getIds(Vertex... _vertexList) {
        List<Vertex> vertexList = new ArrayList<Vertex>();
        for (Vertex v : _vertexList) {
            vertexList.add(v);
        }
        return getIdsByVertex(vertexList);
    }

    public List<Integer> getIds(List<?> list) {
        boolean isString = true;
        boolean isVertex = true;
        for (Object o : list) {
            if (o == null) {
                continue;
            }
            if (!(o instanceof String)) {
                isString = false;
            }
            if (!(o instanceof Vertex)) {
                isVertex = false;
            }
        }
        if (isString) {
            return getIdsByString((List<String>)list);
        } else if (isVertex) {
            return getIdsByVertex((List<Vertex>)list);
        } else {
            return new ArrayList<Integer>();
        }
    }

    private List<Integer> getIdsByString(List<String> values) {
        int valueIndex;
        String value;
        LongList hashCodeList = new LongArrayList();
        List<Integer> idList = new ArrayList<Integer>();
        for (String str : values) {
            hashCodeList.add(computeLongHash(str));
            idList.add(-1);
        }
        int count = vertices.size();
        Vertex vertex;
        for (int i = 0; i < count || !idList.contains(-1); i++) {
            vertex = getVertex(i);
            if (vertex == null || !hashCodeList.contains(vertex.getValueHashCode())) {
                continue;
            }
            valueIndex = hashCodeList.indexOf(vertex.getValueHashCode());
            value = values.get(valueIndex);

            if (value != null) {
                if (value.equals(vertex.getValue())) {
                    idList.set(valueIndex, i);
                }
            } else {
                if (!uniqueVertexValues && vertex.getValue() == null) {
                    idList.set(valueIndex, i);
                }
            }
        }
        return idList;
    }

    private List<Integer> getIdsByVertex(List<Vertex> vertexList) {
        List<Integer> idList = new ArrayList<Integer>();
        for (Vertex v : vertexList) {
            idList.add(-1);
        }
        int count = vertices.size();
        for (int i = 0; i < count || !idList.contains(-1); i++) {
            Vertex vertex = getVertex(i);
            if (vertex == null) {
                continue;
            }
            if (vertexList.contains(vertex)) {
                idList.set(vertexList.indexOf(vertex), i);
            }
        }
        return idList;
    }

    public boolean vertexIdLegit(int vertexId) {
        return vertexId >= 0 && vertexId < vertices.size();
    }

    public int getAnyVertexIdInList(IntList mask, boolean present) {
        for (int i = 0; i < vertices.size(); i++) {
            if(vertices.get(i) != null && !(present ^ mask.contains(i))) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Checks if values of all vertices is unique.
     * If unique vertex values enforced null values are forbidden.
     * Relies on longHashCode of values.
     */
    public boolean vertexValuesUnique() {
        int count = vertices.size();
        LongSet valueSet = new LongOpenHashSet();
        Vertex vertex;
        for (int i = 0; i < count; i++) {
            vertex = getVertex(i);
            /**
             * if vertexValueHashCode != 0 when uniqueVertexValues = true
             */
            if ((!uniqueVertexValues || vertex.getValueHashCode() != 0) && !valueSet.add(vertex.getValueHashCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Safe method to get number of vertices in graph.
     *
     * @return Number of vertices in graph
     */
    public int getNumberOfVertices(){
        return vertexCount;
    }

    /**
     * Safe method to get number of edges in graph.
     *
     * @return Number of edges in graph
     */
    public long getNumberOfEdges(){
        return edgeCount;
    }

    public boolean isDirected(){
        return isDirected;
    }

    public boolean isWeighted(){
        return isWeighted;
    }

    public boolean isConnected(){
        return connectedComponentsCount == 0 || connectedComponentsCount == 1;
    }

    public boolean isCyclic(){
        if (cyclicMayHaveChanged) {
            isCyclic = false;
            GraphAlgorithms.dfs(this);
        }
        return isCyclic;
    }

    public boolean isEmpty(){
        return vertexCount == 0;
    }

    /**
     * Safe method to set state of weight property of current graph.
     *
     * @param state specified state of weight property of the graph
     * @return true if weight state of the graph changed, otherwise false
     */
    public boolean makeWeighted(boolean state) {
        if(state == isWeighted){
            return false;
        }
        if (!state) {
            for (Vertex vertex : vertices) {
                vertex.makeUnweighted();
            }
        }
        isWeighted = state;
        return true;
    }

    /**
     * Safe method to set state of direction property of current graph.
     *
     * @param state specified state of direction property of the graph
     * @return true if direction state of the graph changed, otherwise false
     */
    public boolean makeDirected(boolean state) {
        Vertex vertex;
        if(state == isDirected){
            return false;
        }
        if(state) {
            isDirected = true;
            return true;
        }
        IntList weights = new IntArrayList();
        Int2ObjectMap<IntList> outEdges = new Int2ObjectOpenHashMap<IntList>();
        Int2ObjectMap<Int2ObjectMap<IntList>> edges = new Int2ObjectOpenHashMap<Int2ObjectMap<IntList>>();
        for (int vertexId = 0; vertexId < vertices.size(); vertexId++) {
            vertex = vertices.get(vertexId);
            if (vertex == null) {
                continue;
            }
            for (int i : vertex.getOutEdges()) {
                for (int weight : vertex.getOutWeights(i)) {
                    if (!edgeExists(i, vertexId, weight)) {
                        weights.add(weight);
                    }
                }
                outEdges.put(i, new IntArrayList(weights));
                weights.clear();
            }
            edges.put(vertexId, new Int2ObjectOpenHashMap<IntList>(outEdges));
            outEdges.clear();
        }
        for (Int2ObjectMap.Entry<Int2ObjectMap<IntList>> i : edges.int2ObjectEntrySet()) {
            for (Int2ObjectMap.Entry<IntList> j : i.getValue().int2ObjectEntrySet()) {
                for (int k : j.getValue()) {
                    addEdge(j.getKey(), i.getKey(), k);
                }
            }
        }
        edges.clear();
        isDirected = false;
        return true;
    }

    boolean checkIfDirected() {
        Vertex vertex;
        for (int vertexId = 0; vertexId < vertexCount; vertexId++) {
            vertex = vertices.get(vertexId);
            if (vertex == null) {
                continue;
            }
            for (int i : vertex.getOutEdges()) {
                for (int weight : vertex.getOutWeights(i)) {
                    if (!edgeExists(i, vertexId, weight)) {
                        isDirected = true;
                        return true;
                    }
                }
            }
        }
        isDirected = false;
        return false;
    }

    public Int2ObjectMap<String> getVertexMap() {
        Int2ObjectMap<String> verticesMap = new Int2ObjectOpenHashMap<String>();
        for(int i = 0; i < vertices.size(); i++) {
            verticesMap.put(i, vertices.get(i).getValue());
        }
        return verticesMap;
    }

    public List<Edge> getEdgesList() {
        Vertex vertex;
        IntList visitedVertices = new IntArrayList();
        List<Edge> edges = new ArrayList<Edge>();
        for (int i = 0; i < vertices.size(); i++) {
            vertex = vertices.get(i);
            if (vertex == null) {
                continue;
            }
            for (int id : vertex.getOutEdges()) {
                if (visitedVertices.contains(id)) {
                    continue;
                }
                for (int weight : vertex.getOutWeights(id)) {
                    edges.add(new Edge(i, id, weight));
                }
            }
            if (!isDirected) {
                visitedVertices.add(i);
            }
        }
        return edges;
    }

    /**
     * Safe method to retrieve list of weights of edges connecting specified vertices.
     *
     * @param fromId id of start vertex of edge
     * @param toId id of end vertex of edge
     * @return list of weights of edges, empty list if no edges exist
     */
    public IntList getEdgesWeigts(int fromId, int toId) {
        if(vertexExists(fromId) && vertices.get(fromId).adjacentOut(toId)){
            Vertex from = vertices.get(fromId);
            return from.getOutWeights(toId);
        }
        else{
            return new IntArrayList();
        }
    }

    int getSingleEdgeWeight(int from, int to){
        if(vertexExists(from) && vertices.get(from).adjacentOut(to)) {
            return vertices.get(from).getOutWeights(to).get(0);
        }
        return 0;
    }

    /**
     * Safe method to retrieve list of weights of edges connecting specified vertices.
     *
     * @param idFrom id of start vertex of edge
     * @param idTo id of end vertex of edge
     * @return true if weight of edge was changed, false if edge did not exist
     */
    public boolean setEdgeWeight(int idFrom, int idTo, int weight) {
        if (isAdjacent(idFrom, idTo)) {
            vertices.get(idFrom).setOutWeight(idTo, weight);
            if(!isDirected) {
                vertices.get(idTo).setOutWeight(idFrom, weight);
            }
            return true;
        } else {
            return false;
        }
    }

    void setSingleEdgeWeight(int from, int to, int weight) {

        if(vertexExists(from) && vertexExists(to) && vertices.get(from).adjacentOut(to)){
            vertices.get(from).remOutEdge(to, getSingleEdgeWeight(from, to));
        }
        if (weight != 0) {
            addVertex(from);
            addVertex(to);
            addEdge(from, to, weight);
        }
    }

    Graph prepareForMinCut(){
        Graph newGraph = new Graph();
        Vertex vertex;
        for (int i = 0; i < vertices.size(); i++) {
            vertex = vertices.get(i);
            if (vertex == null) {
                continue;
            }
            newGraph.addVertex(i);
            for (int id : vertex.getOutEdges()) {
                int weight = 0;
                newGraph.addVertex(id);
                for(int w : vertex.getOutWeights(id)) {
                    weight += w;
                }
                newGraph.addEdge(i, id, weight);
            }
        }
        return newGraph;
    }

    void cycleFound() {
        isCyclic = true;
        cyclicMayHaveChanged = false;
    }

    int getVerticesSize() {
        return vertices.size();
    }

    /**
     * Safe method to release unused memory and increase density of graph data arrangement.
     *
     * @return true if graph data arrangement was changed, otherwise false.
     */
    public boolean optimizeMemory() {
        /**
         * calculating number of empty vertices in the end
         * creating table of id's that need to be changed
         *     if no more id replacement needed
         *     remove(0) returns removed value
         *     if found empty vertex in the end, everything after that will also be empty
         * replacing id's of all needed vertices
         * removing unnecessary elements
         */
        int emptyVerticesAtTheEnd = vertices.size();
        for (int i = vertices.size(); i > 0; i--) {
            if (vertices.get(i - 1) != null) {
                emptyVerticesAtTheEnd -= i;
                break;
            }
        }
        boolean somethingChanged = false;
        Int2IntMap replaceMap = new Int2IntOpenHashMap();
        for (int i = vertices.size() - 1 - emptyVerticesAtTheEnd; freeVertices.size() > 0; i--) {
            if(vertices.get(i) != null) {
                if (freeVertices.get(freeVertices.size()-1) >= i) {
                    break;
                }
                int newId = freeVertices.remove(freeVertices.size() - 1);
                replaceMap.put(i, newId);
                updateIdChangeHistory(i, newId);
                emptyVerticesAtTheEnd++;
                somethingChanged = true;
            } else {
                emptyVerticesAtTheEnd++;
            }
        }
        changeVerticesId(replaceMap);
        replaceMap.clear();
        freeVertices.clear();
        int count = vertices.size() - 1;
        for (int i = count; i > count - emptyVerticesAtTheEnd; i--) {
            vertices.remove(i);
        }
        return somethingChanged;
    }

    /**
     * Safe method to change id of needed vertex to specified one
     *
     * @param oldId
     * @param newId
     * @return
     */
    public boolean changeVertexId(int oldId, int newId) {
        if (!vertexIdLegit(oldId) || !vertexIdLegit(newId)) {
            return false;
        }
        if (vertices.get(newId) == null) {
            return replaceVertexId(oldId, newId);
        } else {
            return swapVerticesIds(oldId, newId);
        }
    }

    public boolean changeVerticesId(Int2IntMap replaceMap) {
        Int2IntMap verticesToSwap = new Int2IntOpenHashMap();
        Int2IntMap verticesToIgnore = new Int2IntOpenHashMap();
        for (Int2IntMap.Entry entry : replaceMap.int2IntEntrySet()) {
            if (!vertexIdLegit(entry.getIntKey()) || !vertexIdLegit(entry.getIntValue())) {
                verticesToIgnore.put(entry.getIntKey(), entry.getIntValue());
            }
            if (vertices.get(entry.getValue()) != null) {
                verticesToSwap.put(entry.getIntKey(), entry.getIntValue());
            }
        }

        for (Int2IntMap.Entry entry : verticesToSwap.int2IntEntrySet()) {
            swapVerticesIds(entry.getIntKey(), entry.getIntValue());
            replaceMap.remove(entry.getIntKey());
        }

        for (Int2IntMap.Entry entry : verticesToIgnore.int2IntEntrySet()) {
            replaceMap.remove(entry.getIntKey());
        }

        return replaceVerticesId(replaceMap);
    }

    private boolean replaceVertexId(int oldId, int newId) {
        boolean somethingChanged;
        Vertex vertex;

        vertices.set(newId, vertices.get(oldId));
        vertices.set(oldId, null);

        vertex = vertices.get(newId);
        somethingChanged = vertex.replaceId(oldId, newId);
        for (int i : vertex.getInEdges()) {
            somethingChanged = vertices.get(i).replaceId(oldId, newId) || somethingChanged;
        }
        for (int i : vertex.getOutEdges()) {
           somethingChanged = vertices.get(i).replaceId(oldId, newId) || somethingChanged;
        }
        return somethingChanged;
    }

    private boolean replaceVerticesId(Int2IntMap replaceMap) {
        boolean somethingChanged = false;
        IntSet verticesToReplace = new IntOpenHashSet();
        for (Int2IntMap.Entry entry : replaceMap.int2IntEntrySet()) {
            vertices.set(entry.getIntValue(), vertices.get(entry.getIntKey()));
            vertices.set(entry.getIntKey(), null);
        }
        for (Int2IntMap.Entry entry : replaceMap.int2IntEntrySet()) {
            Vertex vertex = vertices.get(entry.getIntValue());
            somethingChanged = vertex.replaceIds(replaceMap) || somethingChanged;
            for (int i : vertex.getInEdges()) {
                verticesToReplace.add(i);
            }
            for (int i : vertex.getOutEdges()) {
                verticesToReplace.add(i);
            }
        }
        for (int i : verticesToReplace) {
            somethingChanged = vertices.get(i).replaceIds(replaceMap) || somethingChanged;
        }
        return somethingChanged;
    }

    public boolean swapVerticesIds(int v1, int v2) {
        boolean somethingChanged = false;
        vertices.add(null);
        somethingChanged = replaceVertexId(v1, vertices.size()-1);
        somethingChanged = replaceVertexId(v2, v1) || somethingChanged;
        somethingChanged = replaceVertexId(vertices.size()-1, v2) || somethingChanged;
        vertices.remove(vertices.size()-1);
        return somethingChanged;
    }

    public int getVertexCount(){
        return vertexCount;
    }

    IntList getAllVertices() {
        IntList result = new IntArrayList(vertexCount);
        int count = vertexCount;
        for (int i = 0; count > 0; i++) {
            if (vertices.get(i) != null) {
                result.add(i);
                count--;
            }
        }
        return result;
    }

    public static long computeLongHash(String value) {
        if (value == null) {
            return 0L;
        }
        int midlength = value.length()/2;
        String string0 = value.substring(0, midlength);
        String string1 = value.substring(midlength, value.length());
        int hash0 = string0.hashCode();
        int hash1 = string1.hashCode();
        return (((long)hash1) << 32) | (hash0 & 0xffffffffL);
    }

    public List<Integer> getInEdges(int vertexId) {
        if(vertexExists(vertexId)) {
            return vertices.get(vertexId).getInEdges();
        }
        return null;
    }

    public List<Integer> getInEdges(String value) {
        int vertexId = getId(value);
        return getInEdges(vertexId);
    }

    public List<Integer> getOutEdges(int vertexId) {
        if(vertexExists(vertexId)) {
            return vertices.get(vertexId).getOutEdges();
        }
        return null;
    }

    public List<Integer> getOutEdges(String value) {
        int vertexId = getId(value);
        return getOutEdges(vertexId);
    }

    public List<Integer> getEdgeWeights(int idFrom, int idTo) {
        if(vertexExists(idFrom) && vertexExists(idTo)) {
            return vertices.get(idFrom).getOutWeights(idTo);
        }
        return null;
    }

    public List<Integer> getEdgeWeights(String valueFrom, String valueTo) {
        int idFrom = getId(valueFrom);
        int idTo = getId(valueTo);
        return getEdgeWeights(idFrom, idTo);
    }
}

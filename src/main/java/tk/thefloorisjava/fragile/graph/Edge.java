package tk.thefloorisjava.fragile.graph;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by George on 02.10.2015.
 *
 * This class represents an edge: it stores id of start and end vertex, as well as its weight.
 */
public class Edge {

    private int startVertex;
    private int endVertex;
    private int weight;

    public Edge(int v1, int v2){
        this(v1, v2, Graph.DEFAULT_EDGE_WEIGHT);
    }
    public Edge(int v1, int v2, int w){
        startVertex = v1;
        endVertex = v2;
        weight = w;
    }

    public void setStartVertex(int v){
        startVertex = v;
    }
    public int getStartVertex(){
        return startVertex;
    }

    public void setEndVertex(int v){
        endVertex = v;
    }
    public int getEndVertex(){
        return endVertex;
    }

    public void setWeight(int w){
        weight = w;
    }
    public int getWeight(){
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return startVertex == edge.startVertex && endVertex == edge.endVertex && weight == edge.weight;
    }

    @Override
    public int hashCode() {
        int result = startVertex;
        result = 31 * result + endVertex;
        result = 31 * result + weight;
        return result;
    }
}

package tk.thefloorisjava.fragile.graph;

import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import tk.thefloorisjava.fragile.utils.Pair;

import java.util.*;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by George on 01.10.2015.
 */
public class Vertex {

    private String value;
    private long valueHashCode;
    /**
     * key is vertex id, value is number of weights
     */
    private Int2IntMap inEdges;
    /**
     * key is vertex id, value is list of edge weights
     */
    private Int2ObjectMap<IntList> outEdges;

    private int connectedComponentId;

    public Vertex(String value) {
        this.value = value;
        valueHashCode = Graph.computeLongHash(value);
        inEdges = new Int2IntOpenHashMap();
        outEdges = new Int2ObjectOpenHashMap<IntList>();
        connectedComponentId = 0;
    }

    public Vertex(Vertex vertex) {
        String vertexValue = vertex.getValue();
        if (vertexValue == null){
            value = null;
        } else {
            value = new String(vertex.getValue());
        }
        valueHashCode = vertex.getValueHashCode();
        inEdges = new Int2IntOpenHashMap(vertex.getAllInEdges());
        outEdges = new Int2ObjectOpenHashMap<IntList>(vertex.getAllOutEdges());
        connectedComponentId = vertex.getComponentId();
    }

    void addInEdge(int id) {
        if (inEdges.containsKey(id)) {
            inEdges.replace(id, inEdges.get(id) + 1);
        } else {
            inEdges.put(id, 1);
        }
    }

    void addOutEdge(int vertex, int weight) {
        IntList weights = outEdges.get(vertex);
        if (weights != null) {
            weights.add(weight);
        } else {
            weights = new IntArrayList();
            weights.add(weight);
            outEdges.put(vertex, weights);
        }
    }

    boolean remInEdge(int id) {
        if (inEdges.containsKey(id)) {
            int count = inEdges.get(id);
            if (count == 1) {
                inEdges.remove(id);
            } else {
                inEdges.replace(id, count - 1);
            }
            return true;
        } else {
            return false;
        }
    }

    boolean remOutEdge(int vertex, int weight) {
        IntList weights = outEdges.get(vertex);
        if (weights != null) {
            if (!weights.rem(weight)) {
                return false;
            }
            if (weights.isEmpty()) {
                outEdges.remove(vertex);
            }
            return true;
        } else {
            return false;
        }
    }

    public void remAllEdges(int vertex) {
        outEdges.remove(vertex);
    }

    public boolean checkIfWeighted() {
        return checkIfEdgesWeighted(outEdges);
    }

    public boolean checkIfEdgesWeighted(Int2ObjectMap<IntList> edges) {
        for (Int2ObjectMap.Entry<IntList> entry : edges.int2ObjectEntrySet()) {
            IntList weights = entry.getValue();
            for (int i : weights) {
                if (i != Graph.DEFAULT_EDGE_WEIGHT) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setOutWeight(int idTo, int weight) {//ToDo rewrite to make it work with non directed graphs
        outEdges.get(idTo).add(weight);
    }

    public boolean adjacentOut(int idTo) {
        return outEdges.containsKey(idTo);
    }

    public IntList getMentions() {
        IntList mentions = new IntArrayList();
        IntSet hashSet = new IntOpenHashSet();
        for (Int2ObjectMap.Entry<IntList> entry : outEdges.int2ObjectEntrySet()) {
            hashSet.add(entry.getIntKey());
        }
        for (Int2IntMap.Entry entry : inEdges.int2IntEntrySet()) {
            hashSet.add(entry.getIntKey());
        }
        mentions.addAll(hashSet);
        return mentions;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.valueHashCode = Graph.computeLongHash(value);
    }

    public IntList getOutWeights(int vertexId) {
        IntList list = outEdges.get(vertexId);
        if (list == null) {
            list = new IntArrayList();
        }
        return list;
    }

    IntList getInEdges() {
        return new IntArrayList(inEdges.keySet());
    }

    private Int2IntMap getAllInEdges() {
        return inEdges;
    }

    IntList getOutEdges() {
        return new IntArrayList(outEdges.keySet());
    }

    private Int2ObjectMap<IntList> getAllOutEdges() {
        return outEdges;
    }

    public int getComponentId() {
        return connectedComponentId;
    }

    public void setConnectedComponentId(int id) {
        connectedComponentId = id;
    }

    public List<Pair> getPairsMinWeights() throws Exception {
        List<Pair> pairs = new ArrayList<Pair>();

        for (int key : outEdges.keySet()) {
            int min = Integer.MAX_VALUE;
            IntList weights = outEdges.get(key);
            for (int weight : weights)
                if (weight < min) {
                    min = weight;
                }
            Pair way = new Pair(key, min);
            pairs.add(way);
        }
        return pairs;
    }

    /**
     * Safe method to adapt this vertex to unweighted graph.
     */
    public void makeUnweighted() {
        makeEdgesUnweighted(outEdges);
    }

    public void makeEdgesUnweighted(Int2ObjectMap<IntList> edges) {
        IntList weights;
        for (Int2ObjectMap.Entry<IntList> entry : edges.int2ObjectEntrySet()) {
            weights = entry.getValue();
            for (int i = 0; i < weights.size(); i++) {
                weights.set(i, Graph.DEFAULT_EDGE_WEIGHT);
            }
            edges.replace(entry.getKey(), weights);
        }
    }

    /**
     * Unsafe method to replace mentioned id
     */
    boolean replaceId(int oldId, int newId) {
        boolean somethingChanged = false;
        /**
         * replacing incoming edges
         */
        if (remInEdge(oldId)) {
            addInEdge(newId);
            somethingChanged = true;
        }
        /**
         * replacing outgoing edges
         */
        IntList weights = outEdges.remove(oldId);
        if (weights != null) {
            outEdges.put(newId, weights);
            somethingChanged = true;
        }
        return somethingChanged;
    }

    /**
     * Unsafe method to replace all mentioned id's according to specified replacement map
     */
    boolean replaceIds(Int2IntMap replaceMap) {
        boolean somethingChanged = false;
        int newEdge;
        IntList oldEdges = new IntArrayList();
        Int2IntMap newInEdges = new Int2IntOpenHashMap();
        Int2ObjectMap<IntList> newOutEdges = new Int2ObjectOpenHashMap<IntList>();
        /**
         * replacing incoming edges
         */
        int count = inEdges.size();
        oldEdges.addAll(replaceMap.keySet());
        for (Int2IntMap.Entry entry : inEdges.int2IntEntrySet()) {
            newEdge = replaceMap.get(entry.getKey().intValue());
            if (newEdge != replaceMap.defaultReturnValue()) {
                somethingChanged = true;
                newInEdges.put(newEdge, entry.getValue().intValue());
            }
        }
        /**
         * removing all old in edges
         */
        for (int i : oldEdges) {
            inEdges.remove(i);
        }
        /**
         * adding edges with replaced id's
         */
        for (Int2IntMap.Entry entry : newInEdges.int2IntEntrySet()) {
            inEdges.put(entry.getIntKey(), entry.getValue().intValue());
        }
        oldEdges.clear();
        /**
         * replacing outgoing edges
         */
        oldEdges.addAll(replaceMap.keySet());
        for (Int2ObjectMap.Entry<IntList> entry : outEdges.int2ObjectEntrySet()) {
            newEdge = replaceMap.get(entry.getKey().intValue());
            if (newEdge != replaceMap.defaultReturnValue()) {
                somethingChanged = true;
                newOutEdges.put(newEdge, new IntArrayList(entry.getValue()));
            }
        }
        /**
         * removing all old out edges
         */
        for (int i : oldEdges) {
            outEdges.remove(i);
        }
        /**
         * adding edges with replaced id's
         */
        for (Int2ObjectMap.Entry<IntList> entry : newOutEdges.int2ObjectEntrySet()) {
            outEdges.put(entry.getIntKey(), entry.getValue());
        }
        return somethingChanged;
    }

    public long getValueHashCode(){
        return valueHashCode;
    }

    public boolean valueHashCodeEquals(long code){
        return valueHashCode == code;
    }
}
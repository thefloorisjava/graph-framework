package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by George on 29.10.2015.
 */
public class TopoSort {
    @Test
    public void topoSortTest() {
        /**
         * Acceptable conditions
         */
        Graph graph = new Graph();
        graph.addVertices(8);
        graph.makeDirected(true);

        graph.addEdge(0, 3);
        graph.addEdge(0, 4);
        graph.addEdge(1, 3);
        graph.addEdge(2, 4);
        graph.addEdge(2, 7);
        graph.addEdge(3, 5);
        graph.addEdge(3, 6);
        graph.addEdge(3, 7);
        graph.addEdge(4, 6);

        List<Integer> result;
        try {
            result = GraphAlgorithms.topoSort(graph);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }

        List<Integer> valid = new ArrayList<Integer>();

        valid.add(2);
        valid.add(1);
        valid.add(0);
        valid.add(3);
        valid.add(5);
        valid.add(7);
        valid.add(4);
        valid.add(6);

        assertEquals(valid, result);
    }
}
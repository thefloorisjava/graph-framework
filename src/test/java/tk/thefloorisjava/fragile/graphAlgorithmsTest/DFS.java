package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;
import tk.thefloorisjava.fragile.graph.GraphIO;
import tk.thefloorisjava.fragile.utils.GraphExceptions;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 08.11.2015.
 */
public class DFS {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void dfsTest() {
        Graph graph = new Graph();
        graph.makeDirected(true);
        graph.addVertices(6);

        graph.addEdge(0, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 5);
        graph.addEdge(5, 4);
        GraphAlgorithms.dfs(graph, 0);

        List<Integer> postOrder = new ArrayList<Integer>();
        postOrder.add(4);
        postOrder.add(5);
        postOrder.add(2);
        postOrder.add(3);
        postOrder.add(0);
        postOrder.add(1);
        assertEquals(postOrder, graph.getVertexVector3());
    }

    @Test
    public void dfsTest2() {
        Graph graph;
        try{
            URL path = this.getClass().getResource("/graph6.txt");
            graph = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }
        int length = GraphAlgorithms.bfs(graph, 0, 30);
        assertEquals(10, length);
    }

    @Test
    public void dfsOverloadsTest() {
        Graph graph = new Graph();
        graph.makeDirected(true);
        graph.addVertices(6);

        graph.addEdge(0, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 5);
        graph.addEdge(5, 4);
        GraphAlgorithms.dfs(graph, 0);

        List<Integer> postOrder = new ArrayList<Integer>();
        postOrder.add(4);
        postOrder.add(5);
        postOrder.add(2);
        postOrder.add(3);
        postOrder.add(0);
        postOrder.add(1);
        assertEquals(postOrder, graph.getVertexVector3());
    }
}

package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;
import tk.thefloorisjava.fragile.graph.Edge;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 08.11.2015.
 */
public class MST {
    /**
     * Positive test
     */
    @Test
    public void mstTest1() {
        /**
         * Acceptable conditions
         */
        Graph graph = new Graph();
        graph.addVertices(5);

        graph.makeDirected(false);

        graph.addEdge(0, 1, 3);
        graph.addEdge(0, 4, 1);
        graph.addEdge(1, 4, 4);
        graph.addEdge(1, 2, 5);
        graph.addEdge(2, 4, 6);
        graph.addEdge(2, 3, 2);
        graph.addEdge(3, 4, 7);

        List<Edge> result;

        try {
            result = GraphAlgorithms.mstKruskal(graph);
        } catch (Exception e) {
            throw new AssertionError(e.getMessage());
        }

        List<Edge> valid = new ArrayList<Edge>();
        valid.add(new Edge(0, 4, 1));
        valid.add(new Edge(2, 3, 2));
        valid.add(new Edge(0, 1, 3));
        valid.add(new Edge(1, 2, 5));

        assertEquals(result, valid);

    }
    /**
     * Negative test
     */
    @Test
    public void mstTest2(){
        Graph graph = new Graph();
        graph.makeDirected(true);

        graph.addVertices(8);

        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(2, 3);
        graph.addEdge(4, 5);
        graph.addEdge(5, 6);
        graph.addEdge(6, 7);

        try {
            GraphAlgorithms.mstKruskal(graph);
            throw new AssertionError("Expected an Exception: disconnected graph");
        } catch (Exception e) {
        }

        graph.addEdge(3, 4);

        try {
            GraphAlgorithms.mstKruskal(graph);
            throw new AssertionError("Expected an Exception: directed graph");
        } catch (Exception e) {
        }

        graph.makeDirected(false);
        try {
            GraphAlgorithms.mstKruskal(graph);
        } catch (Exception e) {
            throw new AssertionError("Not expected exception:" + e.getMessage());
        }
    }
}

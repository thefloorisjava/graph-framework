package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;
import tk.thefloorisjava.fragile.graph.GraphIO;
import tk.thefloorisjava.fragile.utils.GraphExceptions;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MinCut {
    @Test
    public void minCutTest(){
        try {
            URL path = this.getClass().getResource("/graph2.txt");
            Graph graph = GraphIO.CreateGraph(path.toURI().getPath());

            int result = GraphAlgorithms.minCut(graph);
            assertTrue("MinCut doesn't work!", result == 2);
        }catch(URISyntaxException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.WrongGraphTypeException exception) {
            throw new AssertionError(exception.getMessage());
        }

    }
    @Test
    public void minCutTest1() {
        try {
            URL path = this.getClass().getResource("/graph2.txt");
            Graph graph = GraphIO.CreateGraph(path.toURI().getPath());

            int result = GraphAlgorithms.minCut(graph);
            assertTrue(result == 2);
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        }
    }

    @Test
    public void minCutTest2() {
        try {
            URL path = this.getClass().getResource("/graph1.txt");
            Graph graph = GraphIO.CreateGraph(path.toURI().getPath());

            int result = GraphAlgorithms.minCut(graph);
            throw new AssertionError("Expected an Exception: directed graph");

        } catch (GraphExceptions.WrongGraphTypeException exception) {
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
    }
}

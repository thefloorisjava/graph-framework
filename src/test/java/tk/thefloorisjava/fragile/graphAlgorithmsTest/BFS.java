package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import static org.junit.Assert.*;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;
import tk.thefloorisjava.fragile.graph.GraphIO;
import tk.thefloorisjava.fragile.utils.GraphExceptions;

import java.io.FileNotFoundException;
import java.net.URL;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 08.11.2015.
 */
public class BFS {
    @Test
    public void dfsTest() {
        Graph graph;
        try{
            URL path = this.getClass().getResource("/graph6.txt");
            graph = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }
        int length = GraphAlgorithms.bfs(graph, 0, 30);
        assertEquals(10, length);
    }
}

package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 *
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 08.11.2015.
 */
public class StrongComponents {

    @Test
    public void computingStrongComponents1() throws Exception {
        Graph graph = new Graph();
        graph.makeDirected(true);
        graph.addVertices(10);

        graph.addEdge(0, 3);
        graph.addEdge(6, 7);
        graph.addEdge(4, 9);
        graph.addEdge(7, 8);
        graph.addEdge(8, 5);
        graph.addEdge(1, 5);
        graph.addEdge(5, 1);
        graph.addEdge(2, 7);
        graph.addEdge(8, 1);
        graph.addEdge(6, 1);
        graph.addEdge(8, 6);
        graph.addEdge(3, 4);
        graph.addEdge(2, 5);
        graph.addEdge(6, 2);
        graph.addEdge(5, 6);
        graph.addEdge(9, 7);
        graph.addEdge(9, 0);
        graph.addEdge(1, 8);
        graph.addEdge(1, 6);


        Map<Integer, List<Integer>> answer = GraphAlgorithms.computingStrongComponents(graph);
        Map<Integer, List<Integer>> rightAnswer = new HashMap<Integer, List<Integer>>();
        rightAnswer.put(1, Arrays.asList(0, 9, 4, 3));
        rightAnswer.put(2, Arrays.asList(7, 2, 6, 8, 1, 5));

        assertEquals(rightAnswer, answer);

    }

    @Test
    public void computingStrongComponents2() throws Exception {
        Graph graph1 = new Graph();
        graph1.makeDirected(false);
        Map<Integer, List<Integer>> a = new HashMap<Integer, List<Integer>>();

        try {
            a = GraphAlgorithms.computingStrongComponents(graph1);
            throw new AssertionError("Expected an Exception: disconnected graph");
        } catch (Exception e) {
        }


        graph1.makeDirected(true);
        Map<Integer, List<Integer>> a1 = GraphAlgorithms.computingStrongComponents(graph1);
        Map<Integer, List<Integer>> answer1 = new HashMap<Integer, List<Integer>>();
        assertEquals(answer1, a1);
    }


    @Test
    public void computingStrongComponents3() throws Exception {
        Graph graph1 = new Graph();
        graph1.makeDirected(true);
        graph1.addVertex("0");
        graph1.addVertex("1");
        graph1.addVertex("2");
        graph1.addEdge(2, 1);
        Map<Integer, List<Integer>> a3 = GraphAlgorithms.computingStrongComponents(graph1);
        Map<Integer, List<Integer>> answer3 = new HashMap<Integer, List<Integer>>();
        answer3.put(1, Arrays.asList(2));
        answer3.put(2, Arrays.asList(1));
        answer3.put(3, Arrays.asList(0));
        assertEquals(answer3, a3);
    }
}

package tk.thefloorisjava.fragile.graphAlgorithmsTest;

import org.junit.Test;
import tk.thefloorisjava.fragile.graph.Graph;
import tk.thefloorisjava.fragile.graph.GraphAlgorithms;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by Andry-Bal on 08.11.2015.
 */
public class ShortestPath {

    @Test
    public void shortestPathTest1() {
        Graph graph = new Graph();


        graph.addVertex("0");
        graph.addVertex("1");
        graph.addVertex("2");
        graph.addVertex("3");
        graph.addVertex("4");
        graph.addVertex("5");
        graph.addVertex("6");

        graph.addEdge(0, 1);
        graph.addEdge(0, 3);
        graph.addEdge(1, 2);
        graph.addEdge(1, 4);
        graph.addEdge(2, 5);
        graph.addEdge(2, 6);
        graph.addEdge(3, 4);
        graph.addEdge(3, 6);
        graph.addEdge(5, 6);
        graph.addEdge(4, 5);
        graph.addEdge(0, 1);

        Integer w;
        try {
            w = GraphAlgorithms.shortestPath(graph, 0, 6);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
        Integer weight = 2;
        assertEquals(weight, w);

    }

    @Test
    public void shortestPathTest2() {
        Graph graph1 = new Graph();
        graph1.makeWeighted(true);
        graph1.addVertex("0");
        graph1.addVertex("1");
        graph1.addVertex("2");
        graph1.addVertex("3");
        graph1.addVertex("4");
        graph1.addVertex("5");
        graph1.addVertex("6");


        graph1.makeWeighted(true);
        graph1.addEdge(0, 1, 2);
        graph1.addEdge(0, 3, 4);
        graph1.addEdge(1, 2, 3);
        graph1.addEdge(1, 4, 1);
        graph1.addEdge(2, 5, 2);
        graph1.addEdge(2, 6, 9);
        graph1.addEdge(3, 4, 6);
        graph1.addEdge(3, 6, 3);
        graph1.addEdge(5, 6, 1);
        graph1.addEdge(4, 5, 8);


        Integer w1;
        try {
            w1 = GraphAlgorithms.shortestPath(graph1, 0, 6);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
        List<Integer> postOrder = new ArrayList<Integer>();
        postOrder.add(0);
        postOrder.add(3);
        postOrder.add(6);
        Integer weight1 = 7;

        assertEquals(weight1, w1);
        assertEquals(postOrder, graph1.getVertexVector1());
    }

    @Test
    public void shortestPathTest3() {
        Graph graph1 = new Graph();
        graph1.makeWeighted(true);
        graph1.addVertex("0");

        Integer w2;
        try {
            w2 = GraphAlgorithms.shortestPath(graph1, 0, 10);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }

        Integer weight2 = -1;

        assertEquals(weight2, w2);
    }

    @Test
    public void shortestPathTest4() {
        Graph graph1 = new Graph();
        graph1.makeWeighted(true);
        graph1.addVertex(0);
        graph1.addVertex(7);
        graph1.makeWeighted(true);
        Integer w3;
        try{
            w3 = GraphAlgorithms.shortestPath(graph1, 0, 7);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
        Integer weight3 = -2;
        assertEquals(weight3, w3);
    }


    @Test
    public void shortestPathTest5() {
        Graph graph = new Graph();
        graph.makeWeighted(true);
        graph.addVertex("0");
        Integer w4;
        try{
            w4 = GraphAlgorithms.shortestPath(graph, 0, 0);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
        Integer weight4 = 0;
        assertEquals(weight4, w4);
    }

    @Test
    public void shortestPathTest6() {
        Graph graph = new Graph();
        graph.addVertex("0");
        Integer w4;
        try{
            w4 = GraphAlgorithms.shortestPath(graph, 0, 0);
        } catch (Exception exception) {
            throw new AssertionError(exception.getMessage());
        }
        Integer weight4 = 0;
        assertEquals(weight4, w4);
    }
}

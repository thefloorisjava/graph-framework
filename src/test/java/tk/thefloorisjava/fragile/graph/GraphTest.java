package tk.thefloorisjava.fragile.graph;

import tk.thefloorisjava.fragile.utils.GraphExceptions;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;

/**
 * Copyright 2015 TheFloorIsJava team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class GraphTest {
    @Test
    public void constructorTest(){
        Graph graph1;
        try{
            URL path = this.getClass().getResource("/graph1.txt");
            graph1 = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }
        assertTrue(graph1.isWeighted());
    }

    @Test
    public void cycleDetectionTest(){
        Graph graph = new Graph();
        graph.makeDirected(true);
        graph.addVertices(6);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(2, 3);
        graph.addEdge(3, 4);
        graph.addEdge(4, 5);
        graph.addEdge(0, 5);
        assertTrue("Graph is cyclic!", !graph.isCyclic());
        graph.makeDirected(false);
        assertTrue("Graph is not cyclic!", graph.isCyclic());
        graph.makeDirected(true);
        graph.removeEdge(0, 1);
        graph.removeEdge(1, 2);
        graph.removeEdge(2, 3);
        graph.removeEdge(3, 4);
        graph.removeEdge(4, 5);
        graph.removeEdge(0, 5);
        assertTrue("Graph is cyclic!", !graph.isCyclic());
    }

    @Test
    public void componentDetectionTest(){
        Graph graph = new Graph();
        graph.makeDirected(true);
        graph.addVertices(5);

        graph.addEdge(0, 1);
        graph.addEdge(0, 4);
        graph.addEdge(1, 4);
        graph.addEdge(1, 2);
        graph.addEdge(2, 4);
        graph.addEdge(2, 3);
        graph.addEdge(3, 4);

        assertTrue("Graph connected components count is not equal to 1!", graph.getConnectedComponentsCount() == 1);

        graph.removeEdge(0, 4);
        graph.removeEdge(1, 4);
        graph.removeEdge(1, 2);

        assertTrue("Graph is connected!", !graph.isConnected());
    }

    @Test
    public void isDirectedTest(){
        Graph graph = new Graph();
        graph.addVertices(4);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(2, 1);
        graph.addEdge(2, 3);
        graph.addEdge(3, 2);
        assertTrue("Graph is not directed", graph.isDirected());
        graph.addVertex();
        assertTrue("Graph is not directed", graph.isDirected());
        graph.makeDirected(false);
        assertTrue("Graph is directed",!graph.isDirected());
    }

    @Test
    public void isWeightedTest(){
        Graph graph = new Graph();
        assertTrue(!graph.isWeighted());
        graph.addVertices(4);
        assertTrue(!graph.isWeighted());
        graph.addEdge(0, 1, 2);
        assertTrue(graph.isWeighted());
        graph.addEdge(0, 2, 5);
        graph.addEdge(2, 1, -2);
        graph.addEdge(2, 3, 99);
        graph.addEdge(3, 2, 2323);
        assertTrue(graph.isWeighted());
        graph.makeWeighted(false);
        assertTrue(!graph.isWeighted());
    }

    @Test
    public void isAdjacentTest(){
        Graph graph = new Graph();
        assertFalse(graph.isAdjacent(-2, 23));
        graph.addVertices(3);
        assertFalse(graph.isAdjacent(0, 1));
        graph.addEdge(0,1,6);
        graph.addEdge(0, 2, 10);
        graph.addEdge(1, 2);

        assertTrue(graph.isAdjacent(0, 1));
        assertTrue(graph.isAdjacent(0, 2));
        assertTrue(graph.isAdjacent(1,2));
    }

    @Test
    public void constructorTest2(){
        Graph graph4;
        try{
            URL path = Thread.currentThread().getContextClassLoader().getResource("graph4.txt");
            graph4 = GraphIO.CreateGraph(path.getPath());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }
        assertTrue(graph4.getVertexCount() == 30);
    }


    @Test
    public void memoryReleaseTest(){
        Graph graph;
        try{
            URL path = this.getClass().getResource("/graph3.txt");
            graph = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }

        graph.removeVertex(1);
        graph.removeVertex(4);
        graph.removeVertex(8);
        graph.removeVertex(9);
        graph.removeVertex(10);
        graph.removeVertex(16);
        graph.removeVertex(17);
        graph.removeVertex(18);
        graph.optimizeMemory();
        assertEquals(13, graph.getVertexCount());
        assertTrue(!graph.vertexExists(13));
        assertTrue(!graph.getMentions(0).contains(13));
        assertEquals(1, graph.getConnectedComponentsCount());
    }


    @Test
    public void componentsCountTest(){
        Graph graph = new Graph();
        graph.addVertices(3);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);

        graph.removeVertex(1);
        assertEquals(1, graph.getConnectedComponentsCount());
    }

    @Test
    public void dynamicIdEnabled(){
        Graph graph;
        try{
            URL path = this.getClass().getResource("/graph3.txt");
            graph = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }

        graph.enableDynamicId(true);
        graph.removeVertex(1);
        graph.removeVertex(4);
        graph.removeVertex(8);
        graph.removeVertex(9);
        graph.removeVertex(10);
        assertTrue(graph.peekIdChange(0) == -1);
        assertTrue(graph.peekIdChange(1) == -1);
        assertTrue(graph.peekIdChange(2) == -1);
        assertTrue(graph.peekIdChange(3) == -1);
        assertTrue(graph.peekIdChange(4) == -1);
        assertTrue(graph.peekIdChange(5) == -1);
        assertTrue(graph.peekIdChange(6) == -1);
        assertTrue(graph.peekIdChange(7) == -1);
        assertTrue(graph.peekIdChange(8) == -1);
        assertTrue(graph.peekIdChange(9) == -1);
        assertTrue(graph.peekIdChange(10) == -1);
        assertTrue(graph.peekIdChange(11) == -1);
        assertTrue(graph.peekIdChange(12) == -1);
        assertTrue(graph.peekIdChange(13) == -1);
        assertTrue(graph.peekIdChange(14) == -1);
        assertTrue(graph.peekIdChange(15) == -1);
        assertTrue(graph.peekIdChange(16) >= 0);
        assertTrue(graph.peekIdChange(17) >= 0);
        assertTrue(graph.peekIdChange(18) >= 0);
        assertTrue(graph.peekIdChange(19) >= 0);
        assertTrue(graph.peekIdChange(20) >= 0);
        graph.removeVertex(15);
        assertTrue(graph.peekIdChange(0) == -1);
        assertTrue(graph.peekIdChange(1) == -1);
        assertTrue(graph.peekIdChange(2) == -1);
        assertTrue(graph.peekIdChange(3) == -1);
        assertTrue(graph.peekIdChange(4) == -1);
        assertTrue(graph.peekIdChange(5) == -1);
        assertTrue(graph.peekIdChange(6) == -1);
        assertTrue(graph.peekIdChange(7) == -1);
        assertTrue(graph.peekIdChange(8) == -1);
        assertTrue(graph.peekIdChange(9) == -1);
        assertTrue(graph.peekIdChange(10) == -1);
        assertTrue(graph.peekIdChange(11) == -1);
        assertTrue(graph.peekIdChange(12) == -1);
        assertTrue(graph.peekIdChange(13) == -1);
        assertTrue(graph.peekIdChange(14) == -1);
        assertTrue(graph.peekIdChange(15) == -1);
        assertTrue(graph.peekIdChange(16) >= 0);
        assertTrue(graph.peekIdChange(17) >= 0);
        assertTrue(graph.peekIdChange(18) >= 0);
        assertTrue(graph.peekIdChange(19) >= 0);
        assertTrue(graph.peekIdChange(20) >= 0);
        graph.removeVertex(0);
        assertTrue(graph.peekIdChange(13) == -1);
        assertTrue(graph.peekIdChange(20) == 1);
        graph.removeVertex(1);
        assertTrue(graph.peekIdChange(13) == 1);
        assertTrue(graph.peekIdChange(20) == -1);
        List<Integer> l = graph.getIdChangeHistory();
    }

    @Test
    public void computeLongHashTest() {
        Graph graph = new Graph();
        long code = Graph.computeLongHash("123");
        assertEquals(1601, code >> 32);
    }

    @Test
    public void addEdgeTest() {
        Graph g;
        try{
            URL path = this.getClass().getResource("/test.txt");
            g = GraphIO.CreateGraph(path.getFile());
        } catch (FileNotFoundException exception){
            exception.printStackTrace();
            throw new AssertionError(exception.getMessage());
        } catch (GraphExceptions.InvalidGraphFileFormatException exception) {
            throw new AssertionError(exception.getMessage());
        }

        for (int i = 0; i < 100; i++) {
            g.addEdge(1, 5, 4);
        }

        g.removeEdge(1, 5, 4);
        assertEquals(100, g.getEdgeWeights(1, 5).size());
    }

    @Test
    public void swapVerticesTest() {
        Graph graph = new Graph();
        graph.addVertices(3);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(1, 0);
        graph.swapVerticesIds(0, 1);
        assertEquals(2, graph.getVertex(0).getOutEdges().size());
        assertEquals(true, graph.getVertex(2).getInEdges().contains(0));
    }

    @Test
    public void bigGraphTest() {
        Graph graph = new Graph();
        graph.addVertices(1000);

        for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < 1000; j++) {
                graph.addEdge(i,j);
            }
        }
        int i = 0;
    }
}

